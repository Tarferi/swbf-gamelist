package org.spigot.swbfgl;

public class TeamTranslator {

	public static final String translate(String team) {
		team=team.toLowerCase();
		switch(team) {
			default:
				return team+" (Unrecognized)";
			case "imperium":
				return "Empire";
			case "rebellen":
			case "rebels":
				return "Rebels";
			case "cis":
				return "CIS";
			case "empire":
			case "republic":
				return "Republic";
		}
	}
}
