package org.spigot.swbfgl.catalog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ithief.kuvag.SP.utils.IOUtils.CopyOperation;
import org.spigot.swbfgl.Settings;
import org.spigot.swbfgl.servers.Server;
import org.spigot.swbfgl.utils.URLRetriever;

public class MapManager {

	private static List<Map> maps = new ArrayList<>();
	private static List<String> mapsByCodes = new ArrayList<>();
	private static List<Map> mapsDis = new ArrayList<>();
	private static List<Map> mapsEna = new ArrayList<>();

	private static HashMap<String, List<CatalogMap>> externalMaps = new HashMap<>();
	private static CopyOperation cop;

	static {
		construct();
	}

	private static final String delim0 = new String(new byte[] { 1 });
	private static final String delim1 = new String(new byte[] { 6 });
	private static final String delim2 = new String(new byte[] { 3 });
	private static final String delim3 = new String(new byte[] { 2 });

	public static void loadExternalMaps() {
		String resp = URLRetriever.getURLContent("http://swbf.ithief.net/master.php?action=GET");
		if (resp != null) {
			externalMaps.clear();
			String[] servers = resp.split(delim3);
			for (String s : servers) {
				try {
					String[] data = s.split(delim2);
					String servername = data[0];
					String serverip = data[1];
					String[] maps = data[2].split(delim1);
					String key = serverip + ":" + servername;
					List<CatalogMap> mps = new ArrayList<>();
					for (String m : maps) {
						String[] mdata = m.split(delim0);
						String code = mdata[0];
						String mapname = mdata[1];
						String url = mdata[2];
						CatalogMap map = new CatalogMap(servername, mapname, serverip, code, url);

						mps.add(map);
					}
					externalMaps.put(key, mps);
				} catch (Throwable e) {

				}
			}
		}
	}

	public static void construct() {
		loadMaps();
	}

	private static void loadMaps() {
		maps = MapDetector.getMaps();
		mapsDis = MapDetector.getDisabledMaps();
		mapsEna = MapDetector.getEnabledMaps();
		mapsByCodes.clear();
		for (Map m : maps) {
			mapsByCodes.add(m.getPureMapCode().toLowerCase());
		}
		for (Map m : mapsDis) {
			mapsByCodes.add(m.getPureMapCode().toLowerCase());
		}
		for (Map m : mapsEna) {
			mapsByCodes.add(m.getPureMapCode().toLowerCase());
		}
	}

	public static List<Map> getDisabledMapList() {
		return mapsDis;
	}

	public static List<Map> getEnabledMapList() {
		return mapsEna;
	}

	public static List<Map> getMapList() {
		return maps;
	}

	public static String getServerFolderPath() {
		return getParentPath(Settings.getGamePath());
	}

	private static String getParentPath(String s) {
		s = s.replace("\\", "/");
		if (s.contains("/")) {
			s = s.substring(0, s.lastIndexOf("/"));
		}
		return s;
	}

	public static void reloadMaps() {
		loadMaps();
	}

	public static boolean getMapByCode(String code) {
		code = code.substring(0, code.length() - 1);
		return getMapByPureCode(code);
	}

	public static boolean getMapByPureCode(String code) {
		return mapsByCodes.contains(code.toLowerCase());
	}

	public static void disableMap(Map m) {
		MapDetector.disableMap(m);
	}

	public static void enableMap(Map m) {
		MapDetector.enableMap(m);
	}

	public static String getMapOrigin(Map map) {
		return MapDetector.getMapOrigin(map);
	}

	public static void setMapOrigin(String targetfile, String url) {
		MapDetector.setMapOrigin(targetfile, url);
	}

	public static String getMapNameByCode(String purecode) {
		if (mapsByCodes.contains(purecode)) {
			for (Map m : maps) {
				if (m.getPureMapCode().equals(purecode)) {
					return m.getMapName();
				}
			}
		}
		return null;
	}

	public static boolean hasAllMaps(Server s) {
		String key = s.ip + ":" + s.getServerName();
		synchronized (externalMaps) {
			if (externalMaps.containsKey(key)) {
				List<CatalogMap> lst = externalMaps.get(key);
				for (CatalogMap m : lst) {
					if (!getMapByPureCode(m.getMapCode())) {
						return false;
					}
				}
			}
			return true;
		}
	}

	public static List<CatalogMap> getAllMaps(Server s, Runnable r) {
		synchronized (externalMaps) {
			String key = s.ip + ":" + s.getServerName();
			if (externalMaps.containsKey(key)) {
				return externalMaps.get(key);
			}
		}
		return null;
	}

	public static boolean isMastered(Server s) {
		String key = s.ip + ":" + s.getServerName();
		return externalMaps.containsKey(key);
	}

	public static void setCancellableOperation(CopyOperation op) {
		cop=op;
	}

	public static void unsetCancellableOperation(CopyOperation op) {
		cop=null;
	}
	
	public static boolean hasCancellableOperation() {
		return cop!=null;
	}
	
	public static CopyOperation getCancellableOperation() {
		return cop;
	}
}
