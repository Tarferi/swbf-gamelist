package org.spigot.swbfgl.catalog;

import java.io.Serializable;

public class Map implements Serializable {

	private static final long serialVersionUID = 6181077601346060613L;

	private String code;

	private String lcode;

	private String path;

	private String mapname;

	public void setFolder(String path) {
		this.path = path;
	}

	public String getURLOrigin() {
		if (path != null) {
			return MapManager.getMapOrigin(this);
		}
		return null;
	}

	public String getPath() {
		return path;
	}

	public Map(String code) {
		this.code = code;
		resolveLCode();
	}

	public Map(String code, String mapname) {
		this.code = code;
		this.mapname = mapname;
		resolveLCode();
	}

	public String getMapName() {
		return mapname;
	}

	private void resolveLCode() {
		lcode = code.substring(0, code.length() - 1);
	}

	public String getMapCode() {
		return code;
	}

	public String getPureMapCode() {
		return lcode;
	}
}