package org.spigot.swbfgl.catalog;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;

public class addmeScriptParser {

	public static Map[] getMap(File f) {
		try {
			byte[] data = Files.readAllBytes(f.toPath());
			String d = new String(data);
			int index = d.indexOf("mapluafile") + ("mapluafile".length() + 1);
			ByteBuffer b = ByteBuffer.wrap(data, index, data.length - index);
			b.order(ByteOrder.LITTLE_ENDIAN);
			getString(b);
			// String mapcode = getString(b);
			getString(b);
			String mapname = getString(b);
			index = d.indexOf("AddDownloadableContent") + "AddDownloadableContent".length() + 1;
			b = ByteBuffer.wrap(data, index, data.length - index);
			b.order(ByteOrder.LITTLE_ENDIAN);
			String reinn1 = getString(b);
			String reinn2 = getString(b);
			if (reinn2.isEmpty()) {
				return new Map[] { new Map(reinn1, mapname) };
			} else {
				return new Map[] { new Map(reinn1, mapname), new Map(reinn2, mapname) };
			}
		} catch (Exception e) {
			return null;
		}
	}

	private static String getString(ByteBuffer b) {
		int len = b.getInt();
		byte[] dst = new byte[len];
		b.get(dst);
		return new String(dst).trim();
	}
}
