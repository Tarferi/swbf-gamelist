package org.spigot.swbfgl.catalog;

public interface CatalogHandler {

	public void setOperationFailed();
	
	public void setOperationStarted();

	public void setOperationFinished();

	public void setOperationStopped();

}
