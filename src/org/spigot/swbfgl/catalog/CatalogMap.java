package org.spigot.swbfgl.catalog;

import java.io.File;
import java.nio.file.Files;

import org.ithief.kuvag.SP.utils.IOUtils.ProgressListener;
import org.spigot.swbfgl.extractorAdapter.ExtractorAdapter;

public class CatalogMap {

	private String name;
	private String code;
	private String url;
	private String ip;
	private String mapname;

	public CatalogMap(String sname, String mname, String ip, String code, String url) {
		this.name = sname;
		this.code = code;
		this.url = url;
		this.ip = ip;
		this.mapname = mname;
	}

	public String getIP() {
		return ip;
	}

	public String getServerName() {
		return name;
	}

	public String getMapCode() {
		return code;
	}

	public void get(final CatalogHandler h) {
		h.setOperationStarted();
		try {
			MapsCatalog.getMap(url, new ProgressListener() {

				@Override
				public void setMaximum(long m) {
				}

				@Override
				public void setValue(long v) {
				}

				@Override
				public void stopped() {
					h.setOperationStopped();
				}

				@Override
				public void fail(Throwable e) {
					h.setOperationFailed();
				}

			}, new CatalogOperationHandler() {

				@Override
				public void operationFailed() {
					h.setOperationFailed();
				}

				@Override
				public void operationFinished(File f, String name) {
					File tf = null;
					try {
						String targetdir = MapManager.getServerFolderPath() + "/AddOn/";
						String targetfile = targetdir + "/" + name;
						tf = new File(targetfile);
						Files.copy(f.toPath(), tf.toPath());
						ExtractorAdapter e = new ExtractorAdapter("addme.script");
						if (!e.extract(targetfile, targetdir)) {
							h.setOperationFailed();
							tf.delete();
							return;
						}
						MapManager.setMapOrigin(e.getFile().getParent(), url);
						MapManager.reloadMaps();
						h.setOperationFinished();
					} catch (Throwable e) {
						e.printStackTrace();
						h.setOperationFailed();
						if (tf != null) {
							tf.delete();
						}
					}
				}

			});
		} catch (Exception e) {
			h.setOperationFailed();
		}
	}

	public interface CatalogOperationHandler {

		public void operationFailed();

		public void operationFinished(File f, String name);

	}

	public String getMapName() {
		return mapname;
	}
}
