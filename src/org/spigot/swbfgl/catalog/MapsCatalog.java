package org.spigot.swbfgl.catalog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.ithief.kuvag.SP.utils.IOUtils;
import org.ithief.kuvag.SP.utils.IOUtils.CopyOperation;
import org.ithief.kuvag.SP.utils.IOUtils.ProgressListener;
import org.spigot.swbfgl.MAIN;
import org.spigot.swbfgl.catalog.CatalogMap.CatalogOperationHandler;
import org.spigot.swbfgl.utils.ShaHasher;
import org.spigot.swbfgl.utils.URLRetriever;

public class MapsCatalog {

	protected static void getMap(final String url, final ProgressListener h, final CatalogOperationHandler ch) {
		new Thread() {
			@Override
			public void run() {
				setName("Catalog worker");
				try {
					new MapsCatalog(url, h, ch);
				} catch (Throwable e) {
					ch.operationFailed();
				}
			}
		}.start();
	}

	private CopyOperation op;

	public MapsCatalog(String url, final ProgressListener lh, CatalogOperationHandler ch) {
		retriever = new URLRetriever();
		InputStream input = null;
		FileOutputStream out = null;
		try {
			login();
			downloadDetails d = getMapFileName(url);
			retriever.setGet();
			retriever.setURL(d.getURL());
			input = retriever.commitCurrentGetInput();
			File tmp = MAIN.getTempFile();
			out = new FileOutputStream(tmp);
			op = IOUtils.copy(input, out, lh);
			MapManager.setCancellableOperation(op);
			op.commit();
			MapManager.unsetCancellableOperation(op);
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(out);
			retriever.close();
			ch.operationFinished(tmp, d.getFileName());
		} catch (Throwable e) {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(out);
			retriever.close();
			e.printStackTrace();
			ch.operationFailed();
		}

	}

	private URLRetriever retriever;

	private static final String token_Login1 = "nsubmit=\"hashLoginPassword(this, '";
	private static final String username = "MapCatalogBot3";
	private static final String password = "123456789";

	private void login() throws CatalogException {
		retriever.setURL("http://www.swbfgamers.com/index.php?action=login");
		retriever.setGet();
		retriever.commitCurrent();
		String lastpage = retriever.getLastPageTitle();
		if (!lastpage.equals("Login")) {
			if (lastpage.equals("SWBFGamers - Star Wars Battlefront Maps")) {
				return;
			}
			throw new CatalogException("Failed to fetch login page (" + lastpage + ").");
		}
		String resp = retriever.getResponse();
		int index1 = resp.indexOf(token_Login1);
		if (index1 == -1) {
			throw new CatalogException("Failed to get login token.");
		}
		index1 += token_Login1.length();
		int index2 = resp.indexOf("'", index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to get login token end.");
		}
		String session = resp.substring(index1, index2);
		String passhash;
		try {
			passhash = ShaHasher.getHash(ShaHasher.getHash(username.toLowerCase() + password) + session);
		} catch (Exception e) {
			throw new CatalogException("Failed to calculate password hash.");
		}
		retriever.setURL("http://www.swbfgamers.com/index.php?action=login2");
		retriever.setPost();
		retriever.addPostField("user", username);
		retriever.addPostField("password", "");
		retriever.addPostField("cookielength", "60");
		retriever.addPostField("hash_passwrd", passhash);
		retriever.addPostField("bb2_screener_", System.currentTimeMillis() / 1000 + " 127.0.0.1");
		retriever.commitCurrent();
	}

	private class downloadDetails {
		private String n;
		private String url;

		private downloadDetails(String name, String url) {
			this.n = name;
			this.url = url;
		}

		protected String getURL() {
			return url;
		}

		protected String getFileName() {
			return n;
		}
	}

	private static final String token_DET = "<tr class=\"windowbg2\">";
	private static final String token_DET2 = "<a href=\"";

	private downloadDetails getMapFileName(String url) throws CatalogException {
		retriever.setGet();
		retriever.setURL(url);
		retriever.commitCurrent();
		String resp = retriever.getResponse();
		int index1, index2;
		index1 = resp.indexOf(token_DET);
		if (index1 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		index1 += token_DET.length();
		index1 = resp.indexOf(token_DET2, index1);
		if (index1 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		index1 += token_DET2.length();
		index2 = resp.indexOf("\"", index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		url = resp.substring(index1, index2);
		index1 = resp.indexOf(">", index2);
		if (index1 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		index1 += ">".length();
		index2 = resp.indexOf("<", index1);
		if (index2 == -1) {
			throw new CatalogException("Failed to read map name.");
		}
		String name = resp.substring(index1, index2);
		return new downloadDetails(name, url);
	}

	public static class CatalogException extends Exception {
		private static final long serialVersionUID = -7932540556992865376L;

		private CatalogException(String text) {
			super(text);
		}
	}
}
