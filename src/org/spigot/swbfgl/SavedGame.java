package org.spigot.swbfgl;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

import org.spigot.swbfgl.gui.mainGUI;

@SuppressWarnings("unused")
public class SavedGame {

	public static enum Difficulty {
		Easy, Medium, Hard;
	}

	public static enum Quality {
		Low, Medium, Hard;
	}

	public static enum Volume {
		volume_0, volume_10, volume_20, volume_30, volume_40, volume_50, volume_60, volume_70, volume_80, volume_90, volume_100;
	}

	public static enum Bool {
		True, False;
	}

	public static enum ViewPort {
		V3RD, V1ST;
	}

	private static final String hexStream = "00000000B71DC1046E3B8209D926430DDC7604136B6BC517B24D861A0550471EB8ED08260FF0C922D6D68A2F61CB4B2B649B0C35D386CD310AA08E3CBDBD4F3870DB114CC7C6D0481EE09345A9FD5241ACAD155F1BB0D45BC2969756758B5652C836196A7F2BD86EA60D9B6311105A6714401D79A35DDC7D7A7B9F70CD665E74E0B6239857ABE29C8E8DA191399060953CC0278B8BDDE68F52FBA582E5E66486585B2BBEEF46EABA3660A9B7817D68B3842D2FAD3330EEA9EA16ADA45D0B6CA0906D32D42770F3D0FE56B0DD494B71D94C1B36C7FB06F7C32220B4CE953D75CA28803AF29F9DFBF646BBB8FBF1A679FFF4F63EE143EBFFE59ACDBCE82DD07DEC77708634C06D4730194B043DAE56C539AB0682271C1B4323C53D002E7220C12ACF9D8E1278804F16A1A60C1B16BBCD1F13EB8A01A4F64B057DD00808CACDC90C07AB9778B0B6567C69901571DE8DD475DBDD936B6CC0526FB5E6116202FBD066BF469F5E085B5E5AD17D1D576660DC5363309B4DD42D5A490D0B1944BA16D84097C6A5AC20DB64A8F9FD27A54EE0E6A14BB0A1BFFCAD60BB258B23B69296E2B22F2BAD8A98366C8E41102F83F60DEE87F35DA9994440689D9D662B902A7BEA94E71DB4E0500075E4892636E93E3BF7ED3B6BB0F38C7671F7555032FAE24DF3FE5FF0BCC6E8ED7DC231CB3ECF86D6FFCB8386B8D5349B79D1EDBD3ADC5AA0FBD8EEE00C6959FDCD6D80DB8E6037C64F643296087A858BC97E5CAD8A73EBB04B77560D044FE110C54B383686468F2B47428A7B005C3D66C158E4408255535D43519E3B1D252926DC21F0009F2C471D5E28424D1936F550D8322C769B3F9B6B5A3B26D6150391CBD40748ED970AFFF0560EFAA011104DBDD014949B93192386521D0E562FF1B94BEEF5606DADF8D7706CFCD2202BE2653DEAE6BC1BA9EB0B0668EFB6BB27D701A6E6D3D880A5DE6F9D64DA6ACD23C4DDD0E2C004F6A1CDB3EB60C97E8D3EBDC990FFB910B6BCB4A7AB7DB0A2FB3AAE15E6FBAACCC0B8A77BDD79A3C660369B717DF79FA85BB4921F4675961A163288AD0BF38C742DB081C330718599908A5D2E8D4B59F7AB085440B6C95045E68E4EF2FB4F4A2BDD0C479CC0CD43217D827B9660437F4F460072F85BC176FD0B86684A16476C93300461242DC565E94B9B115E565A1587701918306DD81C353D9F0282205E065B061D0BEC1BDC0F51A69337E6BB52333F9D113E8880D03A8DD097243ACD5620E3EB152D54F6D4297926A9C5CE3B68C1171D2BCCA000EAC8A550ADD6124D6CD2CB6B2FDF7C76EEDBC1CBA1E376D660E7AFF023EA18EDE2EE1DBDA5F0AAA064F4738627F9C49BE6FD09FDB889BEE0798D67C63A80D0DBFB84D58BBC9A62967D9EBBB03E930CADFF97B110B0AF060D71ABDF2B32A66836F3A26D66B4BCDA7B75B8035D36B5B440F7B1";
	private static final String hexEmptyStream = "2352E50F01003000300030003000300030003000300030003000000000000000000000000000000000000000000000000000000000000000004300000044B800390000002E009D002C0000451000004613001C00120052003B0000003C0000003D0000003E0000002100004C000000002200004B000000002D0000000F00000014000000150000003F00000040000000410000004200000043000000440000005700000058000000320000001900000000000000000000000041000000420000000000002000CD001E00CB001100C8001F00D00000000000000000000000000000000000004300000044B800390000002E009D002C0000450000004613001C00120052003B0000003C0000003D0000003E0000002100004B0000004C22000000000000002D0000000F00000014000000150000003F00000040000000410000004200000043000000440000005700000058000000320000001900000000000000000000000041000000420000000000002000CD001E00CB001100C8001F00D00000000000000000000000000000000000004300000044B800390000002E009D002C0000451000004613001C00120052003B0000003C0000003D0000003E0000002100004B0000004C22000000000000002D0000000F00000014000000150000003F00000040000000410000004200000043000000440000005700000058000000320000001900000000000000000000000041000000420000000000000000CD000000CB001100C8001F00D000200000001E00000000000000000000000000A0400000A0400000A04000000000000000000000000000000000050505000000000000000000000000000000000000000000070A080301010000000000000000000000000000000000000000000000000000000000000000000000FF303030303030303030300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000608011E0100010200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100000400030000000000000000000000000000000000000100000001000000010000000000000002000000010101000000003F0000003FF54A193F4ED1F13E3132372E302E302E310000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000088AF7E8";
	private static final int[] dataStream = toIntArray(hexStream);
	private static final byte[] emptyStream = toByteArray(hexEmptyStream);

	private static final int[] toIntArray(String s) {
		int[] dataStream = new int[s.length() / 2];
		for (int i = 0; i < dataStream.length; i++) {
			dataStream[i] = Integer.parseInt(s.substring(i * 2, (i * 2) + 2), 16);
		}
		int size = 0x100;
		int[] data = new int[size];
		int d = 0;
		for (int i = 0; i < size; i++) {
			data[i] = dataStream[d];
			d++;
			data[i] |= dataStream[d] << 0x8;
			d++;
			data[i] |= dataStream[d] << 0x10;
			d++;
			data[i] |= dataStream[d] << 0x18;
			d++;
		}
		return data;
	}

	private static final byte[] toByteArray(String s) {
		byte[] dataStream = new byte[s.length() / 2];
		for (int i = 0; i < dataStream.length; i++) {
			dataStream[i] = (byte) Integer.parseInt(s.substring(i * 2, (i * 2) + 2), 16);
		}
		return dataStream;
	}

	private String nick;
	private final String saveFolder;
	private final byte[] data;

	public SavedGame getClonedProfile(String nick) {
		SavedGame g = new SavedGame(saveFolder, data);
		g.nick = nick;
		g.save();
		return g;
	}

	private SavedGame(String sf, byte[] dt) {
		saveFolder = sf;
		data = dt;
	}

	public SavedGame(File profile) {
		saveFolder = profile.isDirectory() ? profile.getAbsolutePath() : profile.getParentFile().getAbsolutePath();
		byte[] data = null;
		try {
			data = Files.readAllBytes(profile.toPath());
		} catch (Exception e) {
			try {
				File f = new File(saveFolder);
				if (f.isDirectory()) {
					if (f.list().length > 0) {
						System.err.println("Cloning profile " + f.list()[0]);
						data = Files.readAllBytes(new File(saveFolder + "/" + f.list()[0]).toPath());
					}
				}
			} catch (Exception ee) {
				ee.printStackTrace();
			}
		}
		if (data == null) {
			System.err.println("Cloning empty profile");
			data = emptyStream.clone();
		}
		this.data = data;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < nickMaxLength; i++) {
			byte d = data[i + offset_UTF8Nick];
			if (i == 0) {
				break;
			} else {
				sb.append((char) d);
			}
		}
		nick = sb.toString();
	}

	private static final int offset_UTF8Nick = 0x29A;
	private static final int offset_UTF16Nick = 0x006;
	private static final int nickMaxLength = 15;

	private static final int offset_Viewport = 0x032;
	private static final int offset_MusicVolume = 0x270;
	private static final int offset_SFXVolume = 0x271;
	private static final int offset_SpeechVolume = 0x272;
	private static final int offset_MasterVolume = 0x276;
	private static final int offset_Width = 0x376;
	private static final int offset_Height = 0x378;
	private static final int offset_VSync = 0x37b;
	private static final int offset_Antialiasing = 0x37c;
	private static final int offset_TextureDetail = 0x388;
	private static final int offset_TerrainQuality = 0x38c;
	private static final int offset_WaterQuality = 0x390;
	private static final int offset_ParticleQuality = 0x394;
	private static final int offset_ShadowQuality = 0x398;
	private static final int offset_Brightness = 0x3a7;
	private static final int offset_Contrast = 0x3ab;
	private static final int offset_LODDistance = 0x3af;
	private static final int offset_ViewDistance = 0x3b0;
	private static final int offset_Bandwich = 0x31e;
	private static final int offset_TPS = 0x31f;
	private static final int offset_DisplayNetworkPerformanceIcon = 0x320;
	private static final int offset_SearchAllRegions = 0x322;

	public ViewPort getViewPort() {
		return data[offset_Viewport] == 1 ? ViewPort.V1ST : ViewPort.V3RD;
	}

	public void setViewPort(ViewPort v) {
		data[offset_Viewport] = (byte) (v == ViewPort.V1ST ? 1 : 0);
	}

	public int getWidth() {
		return (data[offset_Width + 1] << 8) | (data[offset_Width]);
	}

	public int getHeight() {
		return (data[offset_Height + 1] << 8) | (data[offset_Height]);
	}

	public int getTPS() {
		return data[offset_TPS];
	}

	public void setTPS(int TPS) {
		data[offset_TPS] = (byte) (TPS & 0xff);
	}

	public void setWidth(int width) {
		data[offset_Width] = (byte) (width & 0xff);
		data[offset_Width + 1] = (byte) (width >> 8);
	}

	public void setHeight(int height) {
		data[offset_Height] = (byte) (height & 0xff);
		data[offset_Height + 1] = (byte) (height >> 8);
	}

	private void updateNick() {
		byte[] n = nick.getBytes();
		for (int i = 0; i < nickMaxLength; i++) {
			data[i + offset_UTF8Nick] = 0;
		}
		for (int i = 0; i < nickMaxLength; i++) {
			int nn = i * 2;
			data[nn + offset_UTF16Nick] = 0;
			data[nn + offset_UTF16Nick + 1] = 0;
		}
		for (int i = 0; i < n.length; i++) {
			data[i + offset_UTF8Nick] = n[i];
		}
		for (int i = 0; i < n.length; i++) {
			data[(i * 2) + offset_UTF16Nick] = n[i];
		}
	}

	public void save() {
		updateNick();
		fixFile();
		File f = new File(saveFolder + "/" + nick + ".profile");
		try {
			FileOutputStream s = new FileOutputStream(f);
			s.write(data);
			s.close();
		} catch (Exception e) {
			mainGUI.fail("Failed to save profile.");
		}
	}

	private void fixFile() {
		byte[] header = new byte[0x4];
		byte[] body = new byte[0x454];
		byte[] footer = new byte[0x4];
		System.arraycopy(data, 0x4, body, 0, 0x454); // Copy body from file
		body[0x450] = 0x0;
		footer = checksum(body, 0x454, 0);
		body[0x450] = 0x1;
		byte[] bodyAndFooter = new byte[0x458];
		System.arraycopy(body, 0, bodyAndFooter, 0, 0x454);
		System.arraycopy(footer, 0, bodyAndFooter, 0x454, 0x4);
		header = checksum(bodyAndFooter, 0x458, 0);
		System.arraycopy(header, 0, data, 0, 0x4);
		System.arraycopy(bodyAndFooter, 0, data, 0x4, 0x458);
	}

	public static byte[] checksum(byte[] data, int size, int seed) {
		int acc = ~seed;
		int currentByte;
		int temp;
		for (int i = 0; i < size; i++) {
			currentByte = data[i] & 0xFF;
			temp = acc;
			temp >>>= 0x18;
			temp ^= currentByte;
			currentByte = dataStream[temp];
			acc <<= 0x8;
			acc ^= currentByte;
		}
		acc = ~acc;
		return new byte[] { (byte) (acc), (byte) (acc >> 0x8), (byte) (acc >> 0x10), (byte) (acc >> 0x18) };
	}

	public void setNick(String string) {
		nick = string;
	}
}
