package org.spigot.swbfgl;

import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;

import org.spigot.swbfgl.catalog.MapManager;

public class MapTranslator {
	private final static MapTranslator instance = new MapTranslator();
	private final HashMap<String, Map> maps = new HashMap<>();
	private boolean fileData = false;

	private MapTranslator() {
		try {
			String path = MAIN.getCurrentFolder().getAbsolutePath() + "/" + "maps.txt";
			String data = new String(Files.readAllBytes(new File(path).toPath()));
			data = data.replaceAll("\r", "");
			String[] lines = data.split("\n");
			for (String line : lines) {
				String[] d = line.split(":", 2);
				if (d.length != 2) {
					continue;
				}
				String[] q = d[1].split("\\|", 2);
				if (q.length != 2) {
					continue;
				}
				String code = d[0].trim().toLowerCase();
				String planet = q[0].trim();
				String battle = q[1].trim();
				maps.put(code, new Map(planet, battle, null));
			}
			fileData = true;
			System.out.println("Loaded file maps.txt");
		} catch (Exception e) {
			System.err.println("Faield to load maps.txt\nDefault settings will be used.");
		}
	}

	private static Map loaded(String code) {
		String rc = code.substring(0, code.length() - 1);
		if (instance.maps.containsKey(rc)) {
			Map m = instance.maps.get(rc);
			m.setEra(code);
			return m;
		}
		return null;
	}

	public class Map {
		private String Planet, mapName, Era;

		private Map(String planet, String name, String era) {
			mapName = name;
			Era = era;
			Planet = planet;
		}

		public String getEra() {
			return Era;
		}

		@Override
		public String toString() {
			return Planet == null ? mapName : Planet + ": " + mapName;
		}

		protected void setEra(String code) {
			String last = code.substring(code.length() - 1, code.length());
			Era = "Galactic Civil War";
			switch (last) {
				case "r":
				case "c":
					Era = "Clone Wars";
			}
		}
	}

	public static final Map getMapName(String code) {
		code = code.toLowerCase();
		if (instance.fileData) {
			Map m = loaded(code);
			if (m != null) {
				return m;
			}
		}
		String last = code.substring(code.length() - 1, code.length());
		String era = "Galactic Civil War";
		switch (last) {
			case "r":
			case "c":
				era = "Clone Wars";
		}
		String map = code.substring(0, code.length() - 1);
		String m = "Unknown";
		String p = "Unknown";
		switch (map) {
			case "bes2":
				p = "Bespin";
				m = "Cloud City";
			break;
			case "bes1":
				p = "Bespin";
				m = "Platforms";
			break;
			case "end1":
				p = "Endor";
				m = "Bunker";
			break;
			case "geo1":
				p = "Geonosis";
				m = "Spire";
			break;
			case "hot1":
				p = "Hoth";
				m = "Echo Base";
			break;
			case "kam1":
				p = "Kamino";
				m = "Topica City";
			break;
			case "kam2":
				p = "Kamino";
				m = "Nuricoca City";
			break;
			case "kas2":
				p = "Kashyyyk";
				m = "Docks";
			break;
			case "kas1":
				p = "Kashyyyk";
				m = "Islands";
			break;
			case "nab1":
				p = "Naboo";
				m = "Plains";
			break;
			case "nab2":
				p = "Naboo";
				m = "Theed";
			break;
			case "rhn1":
				p = "Rhen Var";
				m = "Harbour";
			break;
			case "rhn2":
				p = "Rhen Var";
				m = "Citadel";
			break;
			case "tat1":
				p = "Tatooine";
				m = "Dune Sea";
			break;
			case "tat2":
				p = "Tatooine";
				m = "Mos Eisley";
			break;
			case "tat3":
				p = "Tatooine";
				m = "Jabba's Place";
			break;
			case "yav1":
				p = "Yavin 4";
				m = "Temple";
			break;
			case "yav2":
				p = "Yavin 4";
				m = "Arena";
			break;
			case "cor2":
			case "corus2":
				p = "Coruscant";
				m = "Streets";
			break;
			case "cor1":
				p = "Coruscant";
				m = "Temple";
			break;
			case "dag1":
				p = "Dagobah";
				m = "Swamps";
			break;
			case "dan1":
				p = "Dantooine";
				m = "Dust";
			break;
			case "edd1":
				p = "Unknown";
				m = "Eddie's Kastel";
			break;
			case "fel1":
				p = "Felucia";
				m = "Woods";
			break;
			case "hot2":
				p = "Hoth";
				m = "Cave";
			break;
			case "koh1":
				p = "Kohlma";
				m = "Moon of the dead";
			break;
			case "mus1":
				p = "Mustafar";
				m = "Refinery";
			break;
			case "nar1":
				p = "Nar Shaddaa";
				m = "The roof";
			break;
			case "uta1":
				p = "Utapau";
				m = "Sinkhole";
			break;
			default:
				p = null;
				m = MapManager.getMapNameByCode(map);
				if (m == null) {
					return null;
				}
		}
		return instance.new Map(p, m, era);
	}

}
