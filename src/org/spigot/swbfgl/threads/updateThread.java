package org.spigot.swbfgl.threads;

import org.spigot.swbfgl.Settings;
import org.spigot.swbfgl.gameList;
import org.spigot.swbfgl.gui.mainGUI;

public class updateThread extends Thread {

	private static int togo = 0;

	private static final Object syncer = new Object();

	private int lf;

	private static updateThread instance;

	public updateThread() {
		start();
		instance = this;
	}

	public static final void updateDelay(int delayy) {
		synchronized (syncer) {
			Settings.setDelay(delayy);
			togo = 0;
		}
	}

	@Override
	public void run() {
		setName("Update thread");
		synchronized (syncer) {
			while (true) {
				try {
					syncer.wait(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (togo >= Settings.getDelay() && Settings.getDelay() > 0) {
					if (Settings.isUpdatePeriodically()) {
						gameList.update();
					}
					togo = 0;
				} else {
					togo++;
				}
				int finished = togo * 100 / Settings.getDelay();
				if (lf != finished) {
					lf = finished;
					mainGUI.updateDelayer(finished);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static void die() {
		if (instance != null) {
			instance.stop();
		}
	}
}
