package org.spigot.swbfgl.threads;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.spigot.swbfgl.Player;
import org.spigot.swbfgl.gui.mainGUI;
import org.spigot.swbfgl.servers.Server;

public class handleThread {

	private static class handleTarget {
		private final byte[] data;
		private final Server owner;

		private handleTarget(byte[] pack, Server server) {
			data = pack;
			owner = server;
		}
	}

	private final static List<handleTarget> packs = new ArrayList<>();
	private final static Object syncer = new Object();

	static {
		new handleThread();
	}

	private static boolean stuck = false;

	private handleThread() {

		new Thread() {
			@Override
			public void run() {
				setName("Handle Thread");
				while (true) {
					handleTarget pack;
					try {
						synchronized (syncer) {
							if (packs.isEmpty()) {
								stuck = true;
								syncer.wait();
							}
							stuck = false;
							pack = packs.remove(0);
						}
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
					try {
						handle(pack);
					} catch (BufferUnderflowException e) {
						System.err.println("Caught BUE on " + pack.owner.ip + ":" + pack.owner.port);
					} catch (Exception | Error e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	private void handle(handleTarget packd) {
		Server s = packd.owner;
		byte[] data = packd.data;
		ByteBuffer b = ByteBuffer.wrap(data);
		if (b.get() == 0) {
			int diff = b.get() & 0xff;
			if (diff == 0x70) { // binary
				System.err.println("Incompatible status");
			} else if (diff == 0x9f) { // dictionary
				b.get();
				b.get();
				b.get();
				HashMap<String, String> m = new HashMap<>();
				List<Player> players = new ArrayList<>();
				List<String> teams = new ArrayList<>();
				while (b.hasRemaining()) {
					String key = getString(b);
					if (key.length() == 0) { // ArrayList follows
						b.get();
						b.get();
						List<String> fields = new ArrayList<>();
						boolean c = true;
						while (b.hasRemaining() && c) {
							String keyy = getString(b);
							if (keyy.length() == 0) {
								c = false;
							} else {
								fields.add(keyy);
							}
						}
						int fc = fields.size();
						c = true;
						while (b.hasRemaining() && c) {
							String keyy = getString(b);
							if (keyy.length() == 0) {
								c = false;
							} else {
								HashMap<String, String> pl = new HashMap<>();
								pl.put(fields.get(0), keyy);
								for (int i = 1; i < fc; i++) {
									keyy = getString(b);
									pl.put(fields.get(i), keyy);
								}
								players.add(new Player(pl));
							}
						}
						if (b.hasRemaining()) {
							b.get();
							getString(b);
							getString(b);
							b.get();
							teams.add(getString(b));
							getString(b);
							teams.add(getString(b));
						} else {
							System.err.println("Malformed packet");
							teams.add("Team 1");
							teams.add("Team 2");
						}
					} else {
						String value = getString(b);
						m.put(key, value);
					}
				}
				s.setTeams(teams);
				s.setPlayers(players);
				s.setMap(m);
				mainGUI.updateServer(s);
			} else {
				System.err.println("Unknown status " + diff);
			}
		} else {
			System.err.println("Invalid status");
		}
	}

	private static String getString(ByteBuffer buff) {
		StringBuilder sb = new StringBuilder();
		try {
			while (buff.hasRemaining()) {
				byte c = buff.get();
				if (c == 0) {
					break;
				} else {
					sb.append((char) c);
				}
			}
		} catch (BufferUnderflowException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static final void addParseRequest(byte[] data, Server owner) {
		synchronized (syncer) {
			boolean notify = packs.isEmpty();
			packs.add(new handleTarget(data, owner));
			if (notify && stuck) {
				syncer.notify();
			}
		}
	}
}
