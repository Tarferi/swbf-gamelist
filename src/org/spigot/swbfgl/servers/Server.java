package org.spigot.swbfgl.servers;

import java.util.HashMap;
import java.util.List;

import org.spigot.swbfgl.MapTranslator;
import org.spigot.swbfgl.MapTranslator.Map;
import org.spigot.swbfgl.Player;
import org.spigot.swbfgl.catalog.MapManager;
import org.spigot.swbfgl.gui.mainGUI;

public class Server {

	private static final HashMap<Integer, Server> regs = new HashMap<>();

	public final String ip;
	public final int port;
	public final int id;

	private String hostname = "???";
	private String mapname = "???";
	private boolean teamdamage = false;
	private String playercount = "???";
	private String reinforcements = "???";
	private String version = "???";
	private String aiperteam = "???";
	private boolean heroes = false;
	private String aidiff = "???";
	private String spawninvinc = "???";
	private String autoteam = "???";
	private int playerCountInt;

	private boolean customWait = false;
	private int customCount = 0;

	private static final void registerServer(Server s) {
		synchronized (regs) {
			regs.put(s.id, s);
		}
	}

	public final void waitForMore(int num) {
		customWait = true;
		customCount = num;
		mainGUI.updateServer(this);
	}

	public final void disableOwnLimit() {
		customWait = false;
		mainGUI.updateServer(this);
	}

	public static final Server getServerById(int id) {
		synchronized (regs) {
			if (regs.containsKey(id)) {
				return regs.get(id);
			} else {
				return null;
			}
		}
	}

	private Object[][] pobj = new Object[0][0];

	public Object[][] getPlayerObject() {
		return pobj;
	}

	private Map map;

	private List<String> teams;

	private boolean ignored;

	private long lastReceived;
	private long lastSent;

	private boolean mastered = false;

	public void setMastered(boolean b) {
		this.mastered = b;
	}

	private static int ids = 0;

	public Server(String ip, int port) {
		this.ip = ip;
		this.port = port;
		id = ids++;
		registerServer(this);
	}

	private final void decodeStats(HashMap<String, String> map) {
		hostname = map.getOrDefault("hostname", "???");
		mapname = map.getOrDefault("mapname", "???");
		this.map = MapTranslator.getMapName(mapname);
		teamdamage = map.getOrDefault("teamdamage", "???").equals("1");
		String playerCountStr = map.getOrDefault("numplayers", "???");
		playercount = playerCountStr + "/" + map.getOrDefault("maxplayers", "???");
		reinforcements = map.getOrDefault("team1reinforcements", "???") + "/" + map.getOrDefault("team2reinforcements", "???");
		version = map.getOrDefault("gamever", "???");
		aiperteam = map.getOrDefault("numai", "???");
		heroes = map.getOrDefault("heroes", "???").equals("1");
		aidiff = parseAiDiff(map.getOrDefault("aidifficulty", "???"));
		spawninvinc = map.getOrDefault("invincibilitytime", "???");
		autoteam = map.getOrDefault("invincibilitytime", "???").equals("1") ? "Auto Assign" : "Player Select";
		try {
			playerCountInt = Integer.parseInt(playerCountStr);
		} catch (Exception e) {
			playerCountInt = 0;
		}
		mainGUI.verifyPlayerCount(this, customCount, customWait, playerCountInt);
		setMastered(MapManager.isMastered(this));
	}

	public int getPlayerCount() {
		return playerCountInt;
	}

	public String getAutoTeam() {
		return autoteam;
	}

	public String getSpawnInvincibility() {
		return spawninvinc;
	}

	public String getAiDifficulty() {
		return aidiff;
	}

	private String parseAiDiff(String diff) {
		switch (diff) {
			case "1":
				return "Easy";
			case "2":
				return "Medium";
			case "3":
				return "Hard";
			default:
				return "Unknown";
		}
	}

	public String getAiPerTeam() {
		return aiperteam;
	}

	public String getHeroes() {
		return heroes ? "True" : "False";
	}

	public final String getEra() {
		return map == null ? "???" : map.getEra();
	}

	public final String getMapName() {
		return map == null ? "Missing: " + mapname : map.toString();
	}

	public final String getServerName() {
		return hostname;
	}

	public final boolean getFriendlyFire() {
		return teamdamage;
	}

	public final String getPlayerCounts() {
		return customWait ? playercount + "/" + customCount : playercount;
	}

	public final int getCustomCount() {
		return customCount;
	}

	public final String getReinforcements() {
		return reinforcements;
	}

	public final void setMap(HashMap<String, String> map) {
		decodeStats(map);
	}

	public void setPlayers(List<Player> players) {
		int c = players.size();
		pobj = new Object[c][];
		int i = 0;
		for (Player p : players) {
			p.setTeam(teams);
			pobj[i] = new Object[] { p.playerName, p.getTeam(), p.Score, p.Deaths, p.Cps, p.Ping };
			i++;
		}
	}

	public String getVersion() {
		return version;
	}

	public void setTeams(List<String> teams) {
		this.teams = teams;
	}

	public boolean isIgnored() {
		return ignored;
	}

	public boolean hasOwnLimit() {
		return customWait;
	}

	public void setIgnored(boolean i) {
		ignored = i;
	}

	public static void resetIgnored() {
		synchronized (regs) {
			for (Server s : regs.values()) {
				s.ignored = false;
			}
		}
	}

	public static void resetLimits() {
		synchronized (regs) {
			for (Server s : regs.values()) {
				s.disableOwnLimit();
			}
		}
	}

	public final void updatePingSent() {
		lastSent = System.currentTimeMillis();
	}

	public final void updatePingReceive() {
		lastReceived = System.currentTimeMillis();
	}

	public String getPing() {
		long diff = lastReceived - lastSent;
		return diff < 0 ? ":(" : diff + " ms";
	}

	public boolean isMastered() {
		return mastered;
	}
}
