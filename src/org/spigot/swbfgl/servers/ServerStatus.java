package org.spigot.swbfgl.servers;

import java.io.IOException;
import java.net.BindException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.HashMap;

import org.spigot.swbfgl.gui.mainGUI;
import org.spigot.swbfgl.threads.handleThread;

public class ServerStatus {
	private static final int port = 49142;
	private static final DatagramSocket sock;

	private static HashMap<String, Server> map = new HashMap<>();

	private static final Object syncer = new Object();
	private static final byte[] statusSeq = new byte[] { (byte) 0xfe, (byte) 0xfd, 0x00, (byte) 0x9f, 0x12, 0x03, 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff };
	private static Thread runner;
	static {
		DatagramSocket s = null;
		try {
			s = new DatagramSocket(port);
		} catch (BindException e) {
			mainGUI.fail("Failed to bind port " + port + "\n" + "Make sure you only have one\ninstance running at the time.");
			mainGUI.exit();
			s = null;
		} catch (SocketException e) {
			s = null;
			e.printStackTrace();
		}
		sock = s;
	}

	protected static Server getServer(SocketAddress i) {
		String s = i.toString().replace("/", "");
		synchronized (map) {
			if (map.containsKey(s)) {
				return map.get(s);
			} else {
				System.err.println("Error loading server");
			}
		}
		return null;
	}

	private static void registerServer(Server s) {
		synchronized (map) {
			map.put(s.ip + ":" + s.port, s);
		}
	}

	public static final boolean startListener() {
		if (sock != null) {
			synchronized (syncer) {
				if (runner == null) {
					runner = new Thread() {
						@Override
						public void run() {
							byte[] buff = new byte[1024 * 100];
							DatagramPacket rec = new DatagramPacket(buff, buff.length);
							while (true) {
								try {
									sock.receive(rec);
									try {
										handlePacket(rec);
									} catch (Exception e) {
										e.printStackTrace();
									}
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
					};
					runner.start();
				}
			}
			return true;
		} else {
			return false;
		}

	}

	private static void handlePacket(DatagramPacket pack) throws Exception {
		byte[] data = pack.getData();
		int len = pack.getLength();
		byte[] ndata = new byte[len];
		System.arraycopy(data, 0, ndata, 0, len);
		Server s = getServer(pack.getSocketAddress());
		s.updatePingReceive();
		handleThread.addParseRequest(ndata, s);
	}

	@SuppressWarnings("deprecation")
	public static final void stopListener() {
		synchronized (syncer) {
			if (runner != null) {
				runner.stop();
				runner = null;
			}
		}
	}

	private static DatagramPacket getStatusPacket() {
		return new DatagramPacket(statusSeq, statusSeq.length);
	}

	private static void addressPacket(DatagramPacket pack, Server s) {
		pack.setSocketAddress(new InetSocketAddress(s.ip, s.port));
	}

	public static void getStatus(Server s) {
		try {
			registerServer(s);
			DatagramPacket pack = getStatusPacket();
			addressPacket(pack, s);
			s.updatePingSent();
			sock.send(pack);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
