package org.spigot.swbfgl;

import java.util.HashMap;
import java.util.List;

public class Player {

	public final String playerName, Score, Deaths, Ping, Cps;
	private String Team = "???";
	private int teamint;

	public Player(HashMap<String, String> pl) {
		playerName = pl.getOrDefault("player_", "???");
		Score = pl.getOrDefault("score_", "???");
		Deaths = pl.getOrDefault("deaths_", "???");
		Ping = pl.getOrDefault("ping_", "???");
		Cps = pl.getOrDefault("cps_", "???");
		Team = pl.getOrDefault("team_", "???");
		try {
			teamint = Integer.parseInt(Team)-1;
		} catch (Exception e) {
			teamint = 0;
		}
	}
	
	public String getTeam() {
		return Team;
	}

	public void setTeam(List<String> teams) {
		Team =TeamTranslator.translate(teams.get(teamint));
	}

}
