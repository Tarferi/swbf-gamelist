package org.spigot.swbfgl.gui;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.CheckboxMenuItem;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

import org.spigot.swbfgl.MAIN;
import org.spigot.swbfgl.Settings;
import org.spigot.swbfgl.gameList;
import org.spigot.swbfgl.catalog.CatalogMap;
import org.spigot.swbfgl.catalog.MapManager;
import org.spigot.swbfgl.servers.Server;
import org.spigot.swbfgl.servers.ServerStatus;
import org.spigot.swbfgl.threads.updateThread;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;

import net.miginfocom.swing.MigLayout;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class mainGUI extends JFrame {
	private static final long serialVersionUID = -3447464859017994436L;
	private static final Object[] playerTableHeader = new Object[] { "Player Name", "Team", "Kills", "Deaths", "CPS", "Ping" };
	private static final Object[][] emptyDoubleObject = new Object[][] {};
	private JPanel contentPane;
	private JTable table;
	private static mainGUI instance;
	private JScrollPane scrollPane_1;
	private JPanel panel;
	private JPanel panel_1;
	private JTable table_1;
	private JScrollPane scrollPane_2;
	private JScrollPane scrollPane_3;
	private JTable table_2;
	private JScrollPane scrollPane_4;
	private JTable table_3;
	private JPanel panel_2;
	private JLabel lblUpdate;
	private JProgressBar progressBar;
	private JTextField textField;
	private JButton btnSet;
	private CheckboxMenuItem silent;
	private TrayIcon tryico;
	private static boolean disturb = true;
	private MenuItem showhide;

	private static final Image SWBFIcon = MAIN.ImageLoader("ico.png").getScaledInstance(60, 60, Image.SCALE_SMOOTH);
	private static final ImageIcon SWBFImageIcon = new ImageIcon(SWBFIcon);
	private static final URI forum;
	private JLabel lblPlayersToNotify;
	private JTextField textField_1;
	private JButton btnSet_1;
	private JPopupMenu menu;
	private JCheckBoxMenuItem popignored;
	private Server lastPopupServer;

	private static boolean alerted = false;
	private JMenuBar menuBar;
	private JMenu mnMain;
	private JMenu mnHelp;
	private JCheckBoxMenuItem chckbxmntmDoNotDisturb;
	private JMenuItem mntmHide;
	private JMenuItem mntmResetIgnoredServers;
	private JSeparator separator;
	private JMenuItem mntmExit;
	private JMenuItem mntmForum;
	private JSeparator separator_1;
	private JMenuItem mntmAbout;
	private JMenuItem popcustom;
	private JLabel statuslabel;
	private JPanel panel_3;
	private JCheckBoxMenuItem chckbxmntmUpdatePeriodicaly;
	private JCheckBoxMenuItem chckbxmntmStartMinimized;
	private JCheckBoxMenuItem chckbxmntmLoadAtSystem;
	private JMenu mnServers;
	private JMenuItem mntmResetServerLimits;
	private JCheckBoxMenuItem chckbxmntmHideEmptyServers;
	private JMenu mnGame;
	private JMenuItem popconnect;
	private JMenuItem mntmSettings;
	private JMenuItem mntmSounds;
	private JMenuItem mntmMasterServer;

	static {
		URI f;
		try {
			f = new URI(MAIN.forumURI);
		} catch (URISyntaxException e) {
			f = null;
		}
		forum = f;
	}

	public static mainGUI construct() {
		return new mainGUI();
	}

	private void setLookAndFeel() {
		try {
			LookAndFeelInfo[] lnfs = UIManager.getInstalledLookAndFeels();
			for (LookAndFeelInfo ln : lnfs) {
				if (ln.getName().toLowerCase().equals("nimbus")) {
					UIManager.setLookAndFeel(ln.getClassName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public mainGUI() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				exit();
			}
		});
		setTitle("SWBF Servers");
		setIconImage(SWBFIcon);
		setLookAndFeel();
		instance = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 521);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnMain = new JMenu("Main");
		menuBar.add(mnMain);

		chckbxmntmDoNotDisturb = new JCheckBoxMenuItem("Do not disturb");
		chckbxmntmDoNotDisturb.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				disturbChanged(chckbxmntmDoNotDisturb.getState());
			}

		});
		mnMain.add(chckbxmntmDoNotDisturb);

		mntmHide = new JMenuItem("Hide");
		mntmHide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				trayclick();
			}
		});

		chckbxmntmUpdatePeriodicaly = new JCheckBoxMenuItem("Update periodically");
		chckbxmntmUpdatePeriodicaly.setSelected(Settings.isUpdatePeriodically());
		chckbxmntmUpdatePeriodicaly.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setPeriodicUpdate(chckbxmntmUpdatePeriodicaly.isSelected());
			}
		});
		mnMain.add(chckbxmntmUpdatePeriodicaly);

		chckbxmntmStartMinimized = new JCheckBoxMenuItem("Start minimized");
		chckbxmntmStartMinimized.setSelected(Settings.isStartMinimized());
		chckbxmntmStartMinimized.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setStartMinimized(chckbxmntmStartMinimized.isSelected());
			}
		});
		mnMain.add(chckbxmntmStartMinimized);
		if (MAIN.isWindows()) {
			chckbxmntmLoadAtSystem = new JCheckBoxMenuItem("Load at system startup");
			chckbxmntmLoadAtSystem.setSelected(Settings.isLoadAtStartup());
			chckbxmntmLoadAtSystem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					setLoadAtSystemStartup(chckbxmntmLoadAtSystem.isSelected());
				}
			});
			mnMain.add(chckbxmntmLoadAtSystem);
		}
		setLoadAtSystemStartup(Settings.isLoadAtStartup());
		mnMain.addSeparator();
		mnMain.add(mntmHide);

		mntmSounds = new JMenuItem("Sounds");
		mntmSounds.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SoundControl.open();
			}
		});
		mnMain.add(mntmSounds);

		separator = new JSeparator();
		mnMain.add(separator);

		mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainGUI.this.dispatchEvent(new WindowEvent(mainGUI.this, WindowEvent.WINDOW_CLOSING));
			}
		});
		mnMain.add(mntmExit);

		mnServers = new JMenu("Servers");
		menuBar.add(mnServers);

		chckbxmntmHideEmptyServers = new JCheckBoxMenuItem("Hide empty servers");
		chckbxmntmHideEmptyServers.setSelected(Settings.isHideEmpty());
		chckbxmntmHideEmptyServers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				hideEmptyServers(chckbxmntmHideEmptyServers.isSelected());
			}
		});
		mnServers.add(chckbxmntmHideEmptyServers);
		mnServers.addSeparator();
		mntmResetIgnoredServers = new JMenuItem("Reset ignored servers");
		mnServers.add(mntmResetIgnoredServers);

		mntmResetServerLimits = new JMenuItem("Reset server limits");
		mntmResetServerLimits.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetServerLimits();
			}
		});
		mnServers.add(mntmResetServerLimits);

		mntmMasterServer = new JMenuItem("Master server");
		mntmMasterServer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MasterServer.open();
			}
		});
		mnServers.addSeparator();
		mnServers.add(mntmMasterServer);
		mntmResetIgnoredServers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetServers();
			}
		});

		mnGame = new JMenu("Game");
		menuBar.add(mnGame);
		mntmSettings = new JMenuItem("Settings");
		mntmSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GameSettings.showDialog();
			}
		});
		mnGame.add(mntmSettings);

		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		mntmForum = new JMenuItem("Forum");
		mntmForum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gotoForum();
			}
		});
		mnHelp.add(mntmForum);

		separator_1 = new JSeparator();
		mnHelp.add(separator_1);

		mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				openAboutWindow();
			}
		});
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		table = new JTable();
		table.setSelectionForeground(java.awt.Color.BLACK);
		table.setSelectionBackground(java.awt.Color.CYAN);
		table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			private static final long serialVersionUID = -8590770800795971749L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				c.setBackground(getRowColor(row));
				c.setForeground(getRowForegroundColor(row));
				return c;
			}
		});
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3) {
					int row = table.rowAtPoint(e.getPoint());
					if (row >= 0) {
						showContentPaneAt(e.getX(), e.getY(), row);
					}
				}
			}

		});

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			private int last = -1;

			@Override
			public void valueChanged(ListSelectionEvent e) {
				int i = table.getSelectedRow();
				if (last != i) {
					last = i;
					if (i >= 0) {
						updateServerData();
					} else {
						clearServerData();
					}
				}
			}

		});
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setModel(new DefaultTableModel(emptyDoubleObject, new String[] { "#", "Version", "Server name", "Players", "Era", "Map", "Ping" }) {
			private static final long serialVersionUID = 8671577613730738871L;
			Class<?>[] columnTypes = new Class[] { Object.class, String.class, Object.class, Object.class, Object.class, Object.class, String.class };

			public Class<?> getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			@Override
			public boolean isCellEditable(int x, int y) {
				return false;
			}
		});
		RowFilter<DefaultTableModel, Object> rowfilter = new RowFilter<DefaultTableModel, Object>() {

			@Override
			public boolean include(javax.swing.RowFilter.Entry<? extends DefaultTableModel, ? extends Object> entry) {
				boolean d = !Settings.isHideEmpty() || (!entry.getStringValue(3).startsWith("0/") && entry.getStringValue(3).length() > 0);
				return d;
			}

		};

		TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>((DefaultTableModel) table.getModel()) {
			private final Comparator<String> cmp1 = new Comparator<String>() {

				@Override
				public int compare(String e1, String e2) {
					try {
						Integer i1 = Integer.parseInt(e1.split(" ")[0]);
						Integer i2 = Integer.parseInt(e2.split(" ")[0]);
						return i1.compareTo(i2);
					} catch (Exception e) {
					}
					return e1.compareTo(e2);
				}

			};
			private final Comparator<String> cmp2 = new Comparator<String>() {

				@Override
				public int compare(String e1, String e2) {
					try {
						Integer i1 = Integer.parseInt(e1.split("/")[0]);
						Integer i2 = Integer.parseInt(e2.split("/")[0]);
						return i1.compareTo(i2);
					} catch (Exception e) {
					}
					return e1.compareTo(e2);
				}

			};
			private final Comparator<String> cmp3 = new Comparator<String>() {

				@Override
				public int compare(String e1, String e2) {
					try {
						Integer i1 = Integer.parseInt(e1);
						Integer i2 = Integer.parseInt(e2);
						return i1.compareTo(i2);
					} catch (Exception e) {
					}
					return e1.compareTo(e2);
				}

			};

			@Override
			public Comparator<?> getComparator(int column) {
				if (column == 6) {
					return cmp1;
				} else if (column == 3) {
					return cmp2;
				} else if (column == 0) {
					return cmp3;
				} else {
					return super.getComparator(column);
				}
			}
		};
		sorter.setRowFilter(rowfilter);
		table.setRowSorter(sorter);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(38);
		table.getColumnModel().getColumn(2).setPreferredWidth(200);
		table.getColumnModel().getColumn(3).setPreferredWidth(51);
		table.getColumnModel().getColumn(4).setPreferredWidth(100);
		table.getColumnModel().getColumn(5).setPreferredWidth(170);
		table.getColumnModel().getColumn(6).setPreferredWidth(60);
		scrollPane.setViewportView(table);
		JPanel panel_30 = new JPanel();
		panel_30.setLayout(new BorderLayout());
		contentPane.add(panel_30, BorderLayout.SOUTH);
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setPreferredSize(new Dimension(2, 150));
		panel_30.add(scrollPane_1, BorderLayout.CENTER);
		panel = new JPanel();
		scrollPane_1.setViewportView(panel);
		scrollPane_1.setPreferredSize(new Dimension(150, 180));
		panel.setLayout(new MigLayout("", "[320px][420px,grow,fill]", "[178px]"));
		panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(300, 50));
		panel.add(panel_1, "cell 0 0,alignx left,growy");
		panel.setPreferredSize(new Dimension(500, 150));
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.PAGE_AXIS));
		scrollPane_2 = new JScrollPane();
		panel_1.add(scrollPane_2);

		table_1 = new JTable() {
			private static final long serialVersionUID = 1L;

			@Override
			public Class<?> getColumnClass(int column) {
				switch (column) {
					case 0:
						return String.class;
					case 1:
						return String.class;
					case 2:
						return String.class;
					case 3:
						return Boolean.class;
					default:
						return Boolean.class;
				}
			}
		};
		table_1.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null }, }, new String[] { "Reinforcements", "Ai per team", "Heroes", "Friendly Fire" }) {
			private static final long serialVersionUID = -7635845862366757282L;

			@Override
			public boolean isCellEditable(int x, int y) {
				return false;
			}
		});
		table_1.getColumnModel().getColumn(0).setPreferredWidth(92);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(72);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(55);
		scrollPane_2.setViewportView(table_1);

		scrollPane_3 = new JScrollPane();
		panel_1.add(scrollPane_3);

		table_2 = new JTable();
		table_2.setModel(new DefaultTableModel(new Object[][] { { null, null, null }, }, new String[] { "Ai difficulty", "Team assign", "Spawn invincibility" }) {
			private static final long serialVersionUID = -6793870993368967213L;

			@Override
			public boolean isCellEditable(int x, int y) {
				return false;
			}
		});
		table_2.getColumnModel().getColumn(1).setPreferredWidth(95);
		table_2.getColumnModel().getColumn(2).setPreferredWidth(110);
		scrollPane_3.setViewportView(table_2);

		scrollPane_4 = new JScrollPane();
		scrollPane_4.setPreferredSize(new Dimension(420, 50));
		panel.add(scrollPane_4, "cell 1 0,alignx left,growy");

		table_3 = new JTable();
		table_3.setModel(new DefaultTableModel(emptyDoubleObject, playerTableHeader) {
			private static final long serialVersionUID = 318112875310147509L;

			@Override
			public boolean isCellEditable(int x, int y) {
				return false;
			}
		});
		table_3.getColumnModel().getColumn(0).setPreferredWidth(97);
		table_3.getColumnModel().getColumn(4).setPreferredWidth(89);
		scrollPane_4.setViewportView(table_3);

		panel_2 = new JPanel();
		panel_2.setPreferredSize(new Dimension(10, 35));
		contentPane.add(panel_2, BorderLayout.NORTH);
		panel_2.setLayout(new MigLayout("", "[39px][151.00px,fill][108.00][78.00][][][82.00]", "[17px]"));

		lblUpdate = new JLabel("Update:");
		panel_2.add(lblUpdate, "cell 0 0,alignx left,aligny center");

		progressBar = new JProgressBar();
		progressBar.setPreferredSize(new Dimension(120, 20));
		panel_2.add(progressBar, "cell 1 0,alignx center,aligny center");

		textField = new JTextField();
		textField.setText(Settings.getDelay() + "");
		panel_2.add(textField, "cell 2 0,growx");
		textField.setColumns(10);

		btnSet = new JButton("Set");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Settings.isUpdatePeriodically()) {
					String s = textField.getText();
					int val = 10;
					try {
						val = Integer.parseInt(s);
					} catch (Exception e) {
						val = 10;
					}
					if (val < 5 && val != -1) {
						val = 5;
					}
					textField.setText(val + "");
					updateThread.updateDelay(val);
					updateStatusLabel();
				} else {
					gameList.update();
				}
			}
		});
		panel_2.add(btnSet, "cell 3 0,growx");

		lblPlayersToNotify = new JLabel("Minimum players to notify:");
		panel_2.add(lblPlayersToNotify, "cell 4 0,alignx trailing");

		textField_1 = new JTextField();
		textField_1.setText(Settings.getPlayerCount() + "");
		panel_2.add(textField_1, "cell 5 0,growx");
		textField_1.setColumns(10);

		btnSet_1 = new JButton("Set");
		btnSet_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int mp = 2;
				;
				try {
					mp = Integer.parseInt(textField_1.getText());
				} catch (Exception e) {
					mp = 2;
				}
				textField_1.setText(mp + "");
				Settings.setPlayerCount(mp);
				updateStatusLabel();
			}
		});
		panel_2.add(btnSet_1, "cell 6 0,growx");
		if (SystemTray.isSupported()) {
			try {
				tryico = new TrayIcon(SWBFIcon);
				tryico.setImageAutoSize(true);
				PopupMenu menu = new PopupMenu();
				silent = new CheckboxMenuItem("Do not disturb");
				silent.addItemListener(new ItemListener() {

					@Override
					public void itemStateChanged(ItemEvent arg0) {
						disturbChanged(silent.getState());
					}

				});
				disturbChanged(false);
				menu.add(silent);
				showhide = new MenuItem("Hide");
				showhide.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						trayclick();
					}

				});
				menu.add(showhide);
				MenuItem clearservers = new MenuItem("Reset ignored servers");
				clearservers.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						resetServers();
					}

				});
				menu.add(clearservers);
				MenuItem exiter = new MenuItem("Exit");
				exiter.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						mainGUI.this.dispatchEvent(new WindowEvent(mainGUI.this, WindowEvent.WINDOW_CLOSING));
					}

				});
				menu.addSeparator();
				menu.add(exiter);

				tryico.setPopupMenu(menu);
				SystemTray.getSystemTray().add(tryico);
				tryico.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						trayclick();
					}

				});
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
		} else {
			System.err.println("System tray is not supported.");
		}
		menu = new JPopupMenu();
		popignored = new JCheckBoxMenuItem("Ignored");
		popcustom = new JMenuItem("Set custom limit");
		popcustom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (lastPopupServer != null) {
					handleCustomDialog(lastPopupServer);
				}
			}

		});
		popconnect = new JMenuItem("Join");
		popconnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (lastPopupServer != null) {
					runGame(lastPopupServer);
				}
			}

		});
		popignored.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean i = popignored.getState();
				if (lastPopupServer != null) {
					lastPopupServer.setIgnored(i);
					table.repaint();
				}
			}

		});
		menu.add(popignored);
		menu.add(popcustom);
		menu.addSeparator();
		menu.add(popconnect);
		table.add(menu);

		panel_3 = new JPanel();
		panel_30.add(panel_3, BorderLayout.SOUTH);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.X_AXIS));
		statuslabel = new JLabel("Refreshing every " + Settings.getDelay() + " seconds, waiting for " + Settings.getPlayerCount() + " players or more.");
		panel_3.add(statuslabel);
		setPeriodicUpdate(Settings.isUpdatePeriodically());
		if (!Settings.isStartMinimized()) {
			super.setVisible(true);
		}
		setHidden(Settings.isStartMinimized());
		// TODO: End
	}

	private void resetServerLimits() {
		Server.resetLimits();
		table.repaint();
	}

	protected void hideEmptyServers(boolean selected) {
		Settings.setHideEmpty(selected);
		((DefaultTableModel) (table.getModel())).fireTableDataChanged();
	}

	private void setLoadAtSystemStartup(boolean selected) {
		Settings.setLoadAtStartup(selected);
	}

	protected void setStartMinimized(boolean selected) {
		Settings.setStartMinimized(selected);
	}

	private void setPeriodicUpdate(boolean selected) {
		Settings.setUpdatePeriodically(selected);
		progressBar.setVisible(selected);
		textField.setVisible(selected);
		lblUpdate.setVisible(selected);
		btnSet.setText(selected ? "Set" : "Update");
		updateStatusLabel();
	}

	private void handleCustomDialog(final Server server) {
		final JDialog b = new JDialog();
		b.setResizable(false);
		b.setBounds(100, 100, 350, 100);
		JComponent contentPanel = (JComponent) b.getContentPane();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("342px:grow"), }, new RowSpec[] { RowSpec.decode("41px:grow"), RowSpec.decode("28px"), }));
		JPanel panel = new JPanel();
		contentPanel.add(panel, "1, 1, fill, fill");
		panel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("125px"), ColumnSpec.decode("254px"), }, new RowSpec[] { RowSpec.decode("41px"), }));
		JLabel lblInputCustomLimit = new JLabel("Input custom limit:");
		panel.add(lblInputCustomLimit, "1, 1, right, center");
		final JTextField textFieldd = new JTextField();
		if (server.hasOwnLimit()) {
			textFieldd.setText(server.getCustomCount() + "");
		}
		panel.add(textFieldd, "2, 1, left, center");
		textFieldd.setColumns(15);
		JPanel buttonPane = new JPanel();
		contentPanel.add(buttonPane, "1, 2, fill, top");
		buttonPane.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.GROWING_BUTTON_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.GROWING_BUTTON_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.GROWING_BUTTON_COLSPEC, }, new RowSpec[] { FormFactory.LINE_GAP_ROWSPEC, RowSpec.decode("23px"), }));
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					int i = Integer.parseInt(textFieldd.getText());
					server.waitForMore(i);
					b.dispose();
					table.repaint();
				} catch (Exception ee) {
					JOptionPane.showMessageDialog(b, "Invalid value " + textFieldd.getText());
				}
			}

		});
		buttonPane.add(okButton, "1, 2, fill, fill");
		b.getRootPane().setDefaultButton(okButton);
		JButton removeButton = new JButton("Remove custom limit");
		removeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				server.disableOwnLimit();
				b.dispose();
				table.repaint();
			}

		});
		if (server.hasOwnLimit()) {
			buttonPane.add(removeButton, "3, 2, fill, fill");
		}
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				b.dispose();
				table.repaint();
			}

		});
		buttonPane.add(btnCancel, "5, 2, fill, top");
		b.setVisible(true);
	}

	private void openAboutWindow() {
		About.open();
	}

	private void gotoForum() {
		try {
			Desktop.getDesktop().browse(forum);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void showContentPaneAt(int x, int y, int row) {
		Server s = this.getServerOnRow(row);
		if (s != null) {
			this.lastPopupServer = s;
			popignored.setState(s.isIgnored());
			popcustom.setText(s.hasOwnLimit() ? "Change custom limit" : "Set custom limit");
			table.getSelectionModel().setSelectionInterval(row, row);
			menu.show(table, x, y);
		}
	}

	private Server getServerOnRow(int row) {
		synchronized (instance) {
			try {
				String val = (String) table.getValueAt(row, 0);
				if (val != null) {
					return Server.getServerById(Integer.parseInt(val));
				} else {
					return null;
				}
			} catch (Exception | Error e) {
				return null;
			}
		}
	}

	private void resetServers() {
		Server.resetIgnored();
		table.repaint();
	}

	private void disturbChanged(boolean state) {
		disturb = state;
		silent.setState(disturb);
		chckbxmntmDoNotDisturb.setState(disturb);
	}

	private void trayclick() {
		boolean vis = isVisible();
		setVisible(!vis);
		setHidden(vis);
	}

	private final void setHidden(boolean hid) {
		if (hid) {
			requestFocus();
			showhide.setLabel("Show");
			mntmHide.setText("Show");
		} else {
			showhide.setLabel("Hide");
			mntmHide.setText("Hide");
		}
	}

	protected void setServerData(final Server s) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				table_1.setValueAt(s.getReinforcements(), 0, 0);
				table_1.setValueAt(s.getAiPerTeam(), 0, 1);
				table_1.setValueAt(s.getHeroes(), 0, 2);
				table_1.setValueAt(s.getFriendlyFire(), 0, 3);
				table_2.setValueAt(s.getAiDifficulty(), 0, 0);
				table_2.setValueAt(s.getAutoTeam(), 0, 1);
				table_2.setValueAt(s.getSpawnInvincibility(), 0, 2);
				DefaultTableModel model = (DefaultTableModel) table_3.getModel();
				Object[][] data = s.getPlayerObject();
				int[] widths = getPlayerTableColumnWidths();
				model.setDataVector(data, playerTableHeader);
				setPlayerTableColumnWidths(widths);
				table_3.repaint();
			}
		});
	}

	private int[] getPlayerTableColumnWidths() {
		int max = table_3.getColumnCount();
		int[] widths = new int[max];
		TableColumnModel cm = table_3.getColumnModel();
		for (int i = 0; i < max; i++) {
			widths[i] = cm.getColumn(i).getWidth();
		}
		return widths;
	}

	private void setPlayerTableColumnWidths(int[] widths) {
		TableColumnModel cm = table_3.getColumnModel();
		for (int i = 0; i < widths.length; i++) {
			cm.getColumn(i).setWidth(widths[i]);
			cm.getColumn(i).setPreferredWidth(widths[i]);
		}
	}

	protected void clearServerData() {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				table_1.setValueAt("", 0, 0);
				table_1.setValueAt("", 0, 1);
				table_1.setValueAt("", 0, 2);
				table_1.setValueAt(false, 0, 3);
				table_2.setValueAt("", 0, 0);
				table_2.setValueAt("", 0, 1);
				table_2.setValueAt("", 0, 2);
				DefaultTableModel model = (DefaultTableModel) table_3.getModel();
				int[] widths = getPlayerTableColumnWidths();
				model.setDataVector(emptyDoubleObject, playerTableHeader);
				setPlayerTableColumnWidths(widths);
				table_3.repaint();
			}
		});
	}

	private void updateServerData() {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				synchronized (instance) {
					int i = table.getSelectedRow();
					if (i >= 0) {
						Server s = getServerOnRow(i);
						if (s != null) {
							setServerData(s);
						}
					}
				}
			}
		});
	}

	private int getServerRow(Server s, boolean create) {
		synchronized (instance) {
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			int len = model.getRowCount();
			if (s != null) {
				for (int i = 0; i < len; i++) {
					String val = (String) model.getValueAt(i, 0);
					if (val != null) {
						if (val.equals(s.id + "")) {
							return i;
						}
					}
				}
			}
			if (create) {
				model.addRow(new Object[] { "", "", "", "", "", false });
				return len;
			} else {
				return -1;
			}
		}
	}

	private void updateServerSync(final Server s) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				synchronized (instance) {
					int index = getServerRow(s, true);
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.setValueAt(s.id + "", index, 0);
					model.setValueAt(s.getVersion(), index, 1);
					model.setValueAt(s.getServerName(), index, 2);
					model.setValueAt(s.getPlayerCounts(), index, 3);
					model.setValueAt(s.getEra(), index, 4);
					model.setValueAt(s.getMapName(), index, 5);
					model.setValueAt(s.getPing(), index, 6);
					int sid = table.getSelectedRow();
					Server lastselected = null;
					if (sid >= 0) {
						lastselected = getServerOnRow(sid);
					}
					model.fireTableDataChanged();
					if (lastselected != null) {
						setServerData(lastselected);
						table.getSelectionModel().setSelectionInterval(sid, sid);
					}
				}
			}

		});
	}

	public static void updateServer(Server s) {
		instance.updateServerSync(s);
	}

	public static void updateDelayer(final int finished) {
		if (Settings.isUpdatePeriodically()) {
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					if (finished == 100) {
						instance.updateServerData();
					}
					instance.progressBar.setValue(finished);
					instance.progressBar.repaint();
				}
			});
		}
	}

	public static void fail(String string) {
		JOptionPane.showMessageDialog(null, string, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public static void exit() {
		if (instance.tryico != null) {
			SystemTray.getSystemTray().remove(instance.tryico);
		}
		ServerStatus.stopListener();
		updateThread.die();
	}

	public static final void alert(final Server s) {
		if (!disturb && !s.isIgnored() && !alerted) {
			alerted = true;
			instance.tryico.displayMessage("Server available", "There are " + s.getPlayerCounts() + " players on\n" + s.getServerName(), TrayIcon.MessageType.INFO);
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					SoundControl.playNotifycationSound();
					int option = JOptionPane.showOptionDialog(null, "There are " + s.getPlayerCounts() + " players on\n" + s.getServerName(), "StarWars Battlefront Notification", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, SWBFImageIcon, new String[] { "So be it", "Ignore this server", "Do not disturb", "Wait for more", "Join" }, 2);
					SoundControl.stopIt();
					if (instance.tryico != null) {
						SystemTray.getSystemTray().remove(instance.tryico);
						try {
							SystemTray.getSystemTray().add(instance.tryico);
						} catch (AWTException e) {
						}
					}
					alerted = false;
					if (option == 0) {
						// so be it
					} else if (option == 1) {
						s.setIgnored(true);
						// ignore
					} else if (option == 2) {
						instance.disturbChanged(true);
					} else if (option == 3) {
						// wait for more
						int n = s.getPlayerCount() + 1;
						s.waitForMore(n);
					} else if (option == 4) {
						// join
						runGame(s);
					} else {
						// wat?
					}
				}

			});
		}
	}

	public static void verifyPlayerCount(Server server, int customCount, boolean customWait, int playerCountInt) {
		int d = customWait ? customCount : Settings.getPlayerCount();
		if (playerCountInt >= d) {
			alert(server);
		}
	}

	private java.awt.Color getRowForegroundColor(int row) {
		Server s = this.getServerOnRow(row);
		if (s.isMastered()) {
			return java.awt.Color.blue;
		} else {
			return java.awt.Color.black;
		}
	}

	private java.awt.Color getRowColor(int row) {
		boolean selected = table.getSelectedRow() == row;
		Server s = this.getServerOnRow(row);
		if (s.isIgnored()) {
			return selected ? java.awt.Color.orange : java.awt.Color.RED;
		} else if (s.hasOwnLimit()) {
			return selected ? java.awt.Color.WHITE : java.awt.Color.green;
		} else {
			return selected ? java.awt.Color.cyan : row % 2 == 0 ? java.awt.Color.decode("#F0F0F0") : java.awt.Color.decode("#E0E0E0");
		}
	}

	public static final void updateStatusLabel() {
		if (Settings.isUpdatePeriodically()) {
			instance.statuslabel.setText("Refreshing every " + Settings.getDelay() + " seconds, waiting for " + Settings.getPlayerCount() + " players or more.");
		} else {
			instance.statuslabel.setText("Refreshing manually, waiting for " + Settings.getPlayerCount() + " players or more.");
		}
	}

	public static final void runGame(final Server s) {
		Runnable r = new Runnable() {
			@Override
			public void run() {
				instance.disturbChanged(true);
				String game = Settings.getGamePath();
				if (game != null) {
					String ip = s.ip + ":" + s.port;
					String profile = Settings.getProfile();
					List<String> params = new ArrayList<>();
					params.add(game);
					if (profile != null) {
						params.add("/name");
						params.add(profile);
					}
					if (Settings.isGameWindowed()) {
						params.add("/win");
					}
					if (Settings.isGameSkipIntro()) {
						params.add("/nomovies");
					}
					if (Settings.isUnlockFPS()) {
						params.add("/noframelock");
					}
					params.add("/connect");
					params.add(ip);
					String[] prms = new String[params.size()];
					for (int i = 0, o = params.size(); i < o; i++) {
						prms[i] = params.get(i);
					}
					ProcessBuilder pb = null;
					pb = new ProcessBuilder(prms);
					try {
						pb.directory(new java.io.File(game).getParentFile());
						pb.start();
					} catch (Exception e) {
						e.printStackTrace();
						mainGUI.fail("Failed to launch game");
					}

				}
			}
		};
		if (MapManager.hasAllMaps(s)) {
			r.run();
		} else {
			List<CatalogMap> maps = MapManager.getAllMaps(s, r);
			if (maps == null) {
				r.run();
			} else {
				if (maps.size() > 0) {
					List<CatalogMap> toget = new ArrayList<>();
					for (CatalogMap m : maps) {
						if (!MapManager.getMapByPureCode(m.getMapCode())) {
							toget.add(m);
						}
					}
					mapGetterGUI.getMaps(toget, r);
				} else {
					r.run();
				}
			}
		}
	}

	public static Image getSWBFIcon() {
		return SWBFIcon;
	}

	public static Icon getSWBFWindowIcon() {
		return SWBFImageIcon;
	}
}
