package org.spigot.swbfgl.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import org.spigot.swbfgl.catalog.CatalogHandler;
import org.spigot.swbfgl.catalog.CatalogMap;
import org.spigot.swbfgl.catalog.MapManager;
import org.spigot.swbfgl.utils.SuperTable;

import javax.swing.JScrollPane;
import javax.swing.JButton;

public class mapGetterGUI extends JFrame {

	private static final long serialVersionUID = -5399255110739482046L;
	private JPanel contentPane;
	private SuperTable table;
	private JScrollPane scrollPane;
	private JButton btnStartGame;
	private Runnable finalop;
	private JLabel lblDownloadingMaps;
	private JButton btnCancel;

	public static void getMaps(List<CatalogMap> maps, Runnable r) {
		new mapGetterGUI(maps, r);
	}

	private mapGetterGUI(List<CatalogMap> maps, Runnable r) {
		setIconImage(mainGUI.getSWBFIcon());
		setTitle("Map getter");
		this.finalop = r;
		construct();
		Object[][] o = new Object[maps.size()][];
		JProgressBar[] bs = new JProgressBar[o.length];
		CatalogMap[] ms = new CatalogMap[o.length];
		int i = 0;
		for (CatalogMap m : maps) {
			JProgressBar b = new JProgressBar();
			bs[i] = b;
			ms[i] = m;
			o[i] = new Object[] { m.getMapCode(), m.getMapName(), b };
			i++;
		}
		table.setData(o);

		btnStartGame = new JButton("Start game");
		btnStartGame.setEnabled(false);
		btnStartGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				finalop.run();
			}

		});

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				stop();
				dispose();
			}
		});
		contentPane.add(btnCancel, "cell 0 2,grow");
		contentPane.add(btnStartGame, "cell 1 2,grow");
		init(bs, ms);
		setVisible(true);
	}

	protected void stop() {
		if(MapManager.hasCancellableOperation()) {
			MapManager.getCancellableOperation().stop();
		}
	}

	private void init(final JProgressBar[] bs, final CatalogMap[] ms) {
		new Thread() {
			private boolean cnt = true;

			private void get(final int i) {
				CatalogHandler h = new CatalogHandler() {

					@Override
					public void setOperationFailed() {
						bs[i].setIndeterminate(false);
						table.replace(bs[i], new JLabel("Failed to install map."));
						lblDownloadingMaps.setText("Failed to install map.");
						table.repaint();
					}

					@Override
					public void setOperationStarted() {
						bs[i].setIndeterminate(true);
					}

					@Override
					public void setOperationFinished() {
						MapManager.reloadMaps();
						bs[i].setIndeterminate(false);
						bs[i].setValue(100);
						if (i == bs.length - 1) {
							btnStartGame.setEnabled(true);
							lblDownloadingMaps.setText("All maps have been downloaded. You can start game now.");
						} else {
							get(i + 1);
						}
					}

					@Override
					public void setOperationStopped() {
						cnt = false;
					}
				};
				if (cnt) {
					ms[i].get(h);
				}
			}

			@Override
			public void run() {
				get(0);
			}
		}.start();
	}

	public void construct() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 524, 312);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[50%][50%]", "[][grow][]"));

		lblDownloadingMaps = new JLabel("Downloading maps...");
		contentPane.add(lblDownloadingMaps, "cell 0 0");

		table = new SuperTable(new Object[] { "Map code", "Map name", "Progress" }, new String[] { "", "grow", "" });

		scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane, "cell 0 1 2 1,grow");
	}

}
