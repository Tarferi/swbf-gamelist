package org.spigot.swbfgl.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

import org.spigot.swbfgl.Settings;

public class GameSettings extends JDialog {
	private static final long serialVersionUID = 4162874470083054270L;
	private static GameSettings instance;

	private final JPanel contentPanel = new JPanel();
	private JTextField textGamePath;
	private JCheckBox windowedCheck;
	private JCheckBox skipIntroCheck;
	private JComboBox<String> profileCombo;

	public static void showDialog() {
		if (instance == null) {
			instance = new GameSettings();
		} else {
			instance.requestFocus();
		}
	}

	private GameSettings() {
		setTitle("Game settings");
		setIconImage(mainGUI.getSWBFIcon());
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				instance = null;
			}

		});
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 374, 220);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[64.00][145.00][][]", "[][][][][]"));
		JLabel lblGamePath = new JLabel("Game path:");
		contentPanel.add(lblGamePath, "cell 0 0,alignx trailing,growy");
		textGamePath = new JTextField(Settings.getGamePath());
		textGamePath.setEnabled(false);
		textGamePath.setEditable(false);
		contentPanel.add(textGamePath, "cell 1 0,grow");
		textGamePath.setColumns(10);
		JButton btnChange = new JButton("Change");
		btnChange.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Settings.changeGameLocation();
				textGamePath.setText(Settings.getGamePath());
			}

		});
		contentPanel.add(btnChange, "cell 2 0,grow");
		JButton btnDetect = new JButton("Detect");
		btnDetect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String path = Settings.detectGamePath();
				if (path == null) {
					mainGUI.fail("Failed to detect game path.");
				} else {
					Settings.setGamePath(path);
					textGamePath.setText(Settings.getGamePath());
				}
			}

		});
		contentPanel.add(btnDetect, "cell 3 0,grow");
		JLabel lblProfile = new JLabel("Profile:");
		contentPanel.add(lblProfile, "cell 0 1,alignx trailing,growy");
		// TODO: Uncomment
		profileCombo = new JComboBox<>(Settings.getGameProfiles(Settings.getRawProfile()));
		profileCombo.setSelectedItem(Settings.getRawProfile());
		contentPanel.add(profileCombo, "cell 1 1,grow");
		JButton btnCustom = new JButton("Custom");
		btnCustom.setToolTipText("");
		btnCustom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String ret = (String) JOptionPane.showInputDialog(GameSettings.this, "Input custom profile name:", "Custom profile name", JOptionPane.PLAIN_MESSAGE, mainGUI.getSWBFWindowIcon(), null, "");
				if (ret != null) {
					Settings.setProfile(ret);
					profileCombo.removeAllItems();
					String[] items = Settings.getGameProfiles(Settings.getRawProfile());
					for (String item : items) {
						profileCombo.addItem(item);
					}
					profileCombo.setSelectedItem(Settings.getRawProfile());
				}
			}

		});
		contentPanel.add(btnCustom, "cell 2 1");
		JLabel lblWindowed = new JLabel("Windowed:");
		contentPanel.add(lblWindowed, "cell 0 2,alignx right,growy");
		windowedCheck = new JCheckBox("");
		windowedCheck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Settings.setGameWindowed(windowedCheck.isSelected());
			}

		});
		windowedCheck.setSelected(Settings.isGameWindowed());
		contentPanel.add(windowedCheck, "cell 1 2");
		JLabel lblTps = new JLabel("Unlock FPS:");
		contentPanel.add(lblTps, "cell 0 3,alignx right,growy");

		final JCheckBox checkBox = new JCheckBox("");
		checkBox.setSelected(Settings.isUnlockFPS());
		contentPanel.add(checkBox, "cell 1 3");
		JLabel lblSkipIntro = new JLabel("Skip intro:");
		contentPanel.add(lblSkipIntro, "cell 0 4,alignx right,growy");
		skipIntroCheck = new JCheckBox("");
		skipIntroCheck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Settings.setGameSkipIntro(skipIntroCheck.isSelected());
			}

		});
		skipIntroCheck.setSelected(Settings.isGameSkipIntro());
		contentPanel.add(skipIntroCheck, "cell 1 4");
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton okButton = new JButton("OK");
		okButton.setActionCommand("OK");
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Settings.setUnlockFPS(checkBox.isSelected());
				Settings.setProfile((String) profileCombo.getSelectedItem());
				instance = null;
				dispose();
			}

		});
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
		setVisible(true);
	}
}
