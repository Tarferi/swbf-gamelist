package org.spigot.swbfgl.gui;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;

import org.spigot.swbfgl.Settings;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MasterServer extends JDialog {

	private static final long serialVersionUID = 4727335405669443401L;
	private static MasterServer instance;
	private final JPanel contentPanel = new JPanel();
	private JTextField textIP;
	private JTextField textPort;
	private JTextField textDelay;

	public static void open() {
		if (instance == null) {
			instance = new MasterServer();
		} else {
			instance.requestFocus();
		}
	}

	private MasterServer() {
		setTitle("Master server settings");
		setIconImage(mainGUI.getSWBFIcon());
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				instance = null;
			}

		});
		setBounds(100, 100, 369, 180);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[127.00][186.00]", "[][][]"));

		JLabel lblMasterServerIp = new JLabel("Master server IP:");
		contentPanel.add(lblMasterServerIp, "cell 0 0,alignx trailing");

		textIP = new JTextField();
		textIP.setText(Settings.getMasterServerIP());
		contentPanel.add(textIP, "cell 1 0,growx");
		textIP.setColumns(20);

		JLabel lblMasterServerPort = new JLabel("Master server Port:");
		contentPanel.add(lblMasterServerPort, "cell 0 1,alignx trailing");

		textPort = new JTextField();
		contentPanel.add(textPort, "cell 1 1,growx");
		textPort.setColumns(10);
		textPort.setText(Settings.getMasterServerPort() + "");

		JLabel lblSocketDelay = new JLabel("Socket delay:");
		contentPanel.add(lblSocketDelay, "cell 0 2,alignx trailing");

		textDelay = new JTextField();
		textDelay.setToolTipText("Increasing this value slows down the loading, however it is recommended for slow connections.");
		textDelay.setText(Settings.getMasterServerTimeout() + "");
		contentPanel.add(textDelay, "cell 1 2,growx");
		textDelay.setColumns(10);
		JPanel buttonPane = new JPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		buttonPane.setLayout(new MigLayout("", "[47px,grow]", "[23px]"));
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int port;
				try {
					port = Integer.parseInt(textPort.getText());
				} catch (Exception ee) {
					mainGUI.fail("Port must be numeric");
					return;
				}
				if (port <= 0) {
					mainGUI.fail("Port value must be greater than 0");
					return;
				}
				String ip = textIP.getText();
				if (!IPValid(ip) && !DomainValid(ip)) {
					mainGUI.fail("Invalid IP value");
					return;
				}
				int delay;
				try {
					delay = Integer.parseInt(textDelay.getText());
				} catch (Exception ee) {
					mainGUI.fail("Delay must be numeric");
					return;
				}
				Settings.setMasterServerTimeout(delay);
				Settings.setMasterServerIP(ip);
				Settings.setMasterServerPort(port);
				instance = null;
				dispose();
			}
		});
		okButton.setPreferredSize(new Dimension(80, 25));
		okButton.setActionCommand("OK");
		buttonPane.add(okButton, "cell 0 0,alignx center,aligny top");
		JButton defbtn = new JButton("Default");
		defbtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				textIP.setText(Settings.defaultMasterIP);
				textPort.setText(Settings.defaultMasterPort+"");
				textDelay.setText(Settings.defaultMasterDelay+"");
			}

		});
		defbtn.setPreferredSize(new Dimension(80, 25));
		defbtn.setActionCommand("Default");
		buttonPane.add(defbtn, "cell 1 0,alignx center,aligny top");
		getRootPane().setDefaultButton(okButton);
		setVisible(true);
	}

	private static boolean DomainValid(String domain) {
		try {
			String[] d = domain.split("\\.");
			if (d.length < 2) {
				return false;
			}
			if (domain.contains(" ")) {
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private static boolean IPValid(String ip) {
		try {
			if (ip == null || ip.isEmpty()) {
				return false;
			}
			String[] parts = ip.split("\\.");
			if (parts.length != 4) {
				return false;
			}
			for (String s : parts) {
				int i = Integer.parseInt(s);
				if ((i < 0) || (i > 255)) {
					return false;
				}
			}
			if (ip.endsWith(".")) {
				return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
