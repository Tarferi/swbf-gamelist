package org.spigot.swbfgl.gui;

import java.awt.BorderLayout;

import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

import org.spigot.swbfgl.MAIN;
import org.spigot.swbfgl.Settings;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class SoundControl extends JDialog {

	private static final long serialVersionUID = 6033874190880736443L;
	private static SoundControl instance;
	private static Sounder sounder;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private static final Object syncer = new Object();

	public static void open() {
		if (instance == null) {
			instance = new SoundControl();
		} else {
			instance.requestFocus();
		}
	}

	public SoundControl() {
		this.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				instance = null;
			}

		});
		setBounds(100, 100, 504, 155);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][242.00][]", "[][][]"));
		JLabel lblNotificationSound = new JLabel("Notification sound:");
		contentPanel.add(lblNotificationSound, "cell 0 0,alignx trailing");
		textField = new JTextField();
		textField.setEditable(false);
		File f = Settings.getNotifySound();
		String str;
		if (f == null) {
			str = "";
		} else {
			str = f.getAbsolutePath();
		}
		textField.setText(str);
		contentPanel.add(textField, "cell 1 0,growx");
		textField.setColumns(10);
		JButton btnChange = new JButton("Change");
		btnChange.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String file = getFile();
				if (file != null) {
					textField.setText(file);
				}
			}

		});
		setTitle("Sound control");
		setIconImage(mainGUI.getSWBFIcon());
		contentPanel.add(btnChange, "cell 2 0,grow");
		JLabel lblLoopUntilClosed = new JLabel("Loop until closed:");
		contentPanel.add(lblLoopUntilClosed, "cell 0 1,alignx right");
		final JCheckBox checkBox = new JCheckBox("");
		checkBox.setSelected(Settings.isNotifySoundLoop());
		contentPanel.add(checkBox, "cell 1 1");
		JLabel lblSoundNotificationEnabled = new JLabel("Sound notification enabled:");
		contentPanel.add(lblSoundNotificationEnabled, "cell 0 2,alignx right");
		final JCheckBox checkBox_1 = new JCheckBox("");
		checkBox_1.setSelected(Settings.getNotifySound() != null);
		contentPanel.add(checkBox_1, "cell 1 2");
		JPanel buttonPane = new JPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		buttonPane.setLayout(new MigLayout("", "[122.00px,grow]", "[23px]"));
		JButton okButton = new JButton("OK");
		okButton.setPreferredSize(new Dimension(90, 25));
		okButton.setActionCommand("OK");
		okButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (checkBox_1.isSelected()) {
					File f = new File(textField.getText());
					if (f.exists()) {
						Settings.setNotifySound(f);
						Settings.setNotifySoundLoop(checkBox.isSelected());
						dispose();
					} else {
						mainGUI.fail("Sound file was not found.");
					}
				} else {
					Settings.setNotifySound(null);
					dispose();
				}
				instance = null;
			}

		});
		buttonPane.add(okButton, "cell 0 0,alignx center,aligny top");
		getRootPane().setDefaultButton(okButton);
		setVisible(true);
	}

	private final String getFile() {
		File ff = Settings.getNotifySound();
		String parent = null;
		if (ff != null) {
			parent = ff.getParentFile().getAbsolutePath();
		} else {
			parent = MAIN.getCurrentFolder().getAbsolutePath();
			File fff = new File(parent + "/Sounds");
			if (fff.exists()) {
				parent = fff.getAbsolutePath();
			}
		}
		JFileChooser fc = new JFileChooser(parent);
		FileFilter[] filters = fc.getChoosableFileFilters();
		for (FileFilter filter : filters) {
			fc.removeChoosableFileFilter(filter);
		}
		fc.addChoosableFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {
				String name = f.getName().toLowerCase();
				return f.isDirectory() || name.endsWith("mp3") || name.endsWith(".wav");
			}

			@Override
			public String getDescription() {
				return "Sound files";
			}

		});
		int ret = fc.showOpenDialog(null);
		if (ret == JFileChooser.APPROVE_OPTION) {
			File f = fc.getSelectedFile();
			if (f != null) {
				if (f.exists()) {
					return f.getAbsolutePath();
				}
			}
		}
		return null;
	}

	public static final void playNotifycationSound() {
		if (Settings.getNotifySound() != null) {
			final File f = Settings.getNotifySound();
			if (f.exists()) {
				synchronized (syncer) {
					if (sounder == null) {
						sounder = new Sounder(f);
					}
				}
			}
		}
	}

	public static final void stopIt() {
		synchronized (syncer) {
			if (sounder != null) {
				sounder.kill();
				sounder = null;
			}
		}
	}

	private static class Sounder extends Thread {
		private MediaPlayer clip;
		private boolean br = true;
		private File f;
		static {
			new JFXPanel();
		}

		protected Sounder(File f) {
			this.f = f;

			start();
		}

		public void kill() {
			if (clip != null) {
				br = false;
				clip.stop();
			}
		}

		@Override
		public void run() {
			try {
				setName("Notification sound thread");
				Media m = new Media(f.toURI().toString());
				clip = new MediaPlayer(m);
				if (Settings.isNotifySoundLoop()) {
					clip.setOnEndOfMedia(new Runnable() {

						@Override
						public void run() {
							if (br) {
								clip.seek(Duration.ZERO);
								clip.play();
							}
						}

					});
				}
				clip.play();
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Failed to play notifycation sound.");
			}
		}
	}
}
