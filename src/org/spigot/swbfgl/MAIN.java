package org.spigot.swbfgl;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.spigot.swbfgl.extractorAdapter.ExtractorAdapter;
import org.spigot.swbfgl.gui.mainGUI;

public class MAIN {

	private static enum SystemType {
		Windows, Linux, Unknown;
	};

	public static final String forumURI = "http://www.swbfgamers.com/";
	public static final String version = "1.21";

	private static final SystemType OS = detectOS();

	public static final boolean isWindows() {
		return OS == SystemType.Windows;
	}

	public static final boolean isLinux() {
		return OS == SystemType.Linux;
	}

	private static final SystemType detectOS() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.startsWith("window")) {
			return SystemType.Windows;
		} else if (os.startsWith("linux")) {
			return SystemType.Linux;
		}
		return SystemType.Unknown;
	}

	public static void main(String[] args) {
		if (ExtractorAdapter.loadLibrary()) {
			load();
		}
	}

	public static void load() {
		mainGUI.construct();
		new gameList();
	}

	public static final Image ImageLoader(String filename) {
		Image qg = null;
		try {
			URL src = mainGUI.class.getResource(filename);
			if (src != null) {
				qg = ImageIO.read(src);
			} else {
				qg = new BufferedImage(20, 20, 1);
				System.err.println("NULL IMAGE " + filename);
			}
		} catch (Exception e) {
			e.printStackTrace();
			qg = new BufferedImage(20, 20, 1);
		}
		return qg;
	}

	public static String getCurrentFile() throws URISyntaxException {
		return new java.io.File(MAIN.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getAbsolutePath();
	}

	public static java.io.File getCurrentFolder() {
		try {
			return new java.io.File(MAIN.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			mainGUI.fail("Failed to find self");
			return null;
		}
	}

	private static final File tmpfolder = new File(getCurrentFolder().getAbsolutePath() + "/tmp/");

	public static File getTempFile() {
		tmpfolder.mkdirs();
		File f = new File(tmpfolder.getAbsolutePath() + "/" + UUID.randomUUID().toString());
		return f;
	}

	public static String getLibraryURI() {
		return "http://swbf.ithief.net/Extractor.jar";
	}

}
