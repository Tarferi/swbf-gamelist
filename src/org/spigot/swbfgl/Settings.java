package org.spigot.swbfgl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.spigot.swbfgl.gui.mainGUI;

public class Settings implements Serializable {
	private static final long serialVersionUID = 8738668688782072816L;

	private static final String settingsFile = "Settings.bin";

	public static final String defaultMasterIP = "swbfronpc4.gameshare.info";
	public static final int defaultMasterPort = 28910;
	public static final int defaultMasterDelay = 500;
	
	private static Settings instance = new Settings();
	private static SavedGame lastProfileUsed;

	private Boolean gameWindowed = false;
	private Boolean gameSkipIntro = true;
	private Integer delay = 10;
	private Integer playerCount = 2;
	private Boolean loadAtStartup = false;
	private Boolean updatePeriodically = true;
	private Boolean startMinimized = false;
	private Boolean hideEmpty = false;
	private String gamePath;
	private String profile;
	private File notifySound;
	private Boolean notifySoundLoop = false;
	private Boolean unlockFPS = true;
	private String masterServerIP = "swbfronpc4.gameshare.info";
	private Integer masterServerPort = 28910;
	private Integer masterServerTimeout = 500;

	private static void commitChanges(Settings data) {
		try {
			Field[] fields = data.getClass().getDeclaredFields();
			for (Field f : fields) {
				int mod = f.getModifiers();
				if (!Modifier.isFinal(mod) && !Modifier.isStatic(mod)) {
					if (f.get(data) == null) {
						Object no = f.get(instance);
						if (no == null) {
							System.err.println("Null value for " + f.getName() + ".");
						} else {
							System.err.println("Setting " + f.getName() + " to default value (" + f.get(instance) + ").");
							f.set(data, no);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getMasterServerIP() {
		return instance.masterServerIP;
	}

	public static int getMasterServerPort() {
		return instance.masterServerPort;
	}

	public static int getMasterServerTimeout() {
		return instance.masterServerTimeout;
	}

	public static void setMasterServerIP(String masterServerIP) {
		instance.masterServerIP = masterServerIP;
		saveSettings();
	}

	public static void setMasterServerPort(int masterServerPort) {
		instance.masterServerPort = masterServerPort;
		saveSettings();
	}

	public static void setMasterServerTimeout(int masterServerTimeout) {
		instance.masterServerTimeout = masterServerTimeout;
		saveSettings();
	}

	public static boolean isUnlockFPS() {
		return instance.unlockFPS;
	}

	public static void setUnlockFPS(boolean unlockFPS) {
		instance.unlockFPS = unlockFPS;
		saveSettings();
	}

	public static boolean isNotifySoundLoop() {
		return instance.notifySoundLoop;
	}

	public static void setNotifySoundLoop(boolean notifySoundLoop) {
		instance.notifySoundLoop = notifySoundLoop;
		saveSettings();
	}

	public static File getNotifySound() {
		return instance.notifySound;
	}

	public static void setNotifySound(File notifySound) {
		instance.notifySound = notifySound;
		saveSettings();
	}

	public static int getPlayerCount() {
		return instance.playerCount;
	}

	public static boolean isLoadAtStartup() {
		return instance.loadAtStartup;
	}

	public static String getSettingsfile() {
		return settingsFile;
	}

	public static void setDelay(int delay) {
		instance.delay = delay;
		saveSettings();
	}

	public static void setPlayerCount(int playerCount) {
		instance.playerCount = playerCount;
		saveSettings();
	}

	public static void setLoadAtStartup(boolean loadAtStartup) {
		instance.loadAtStartup = loadAtStartup;
		if (loadAtStartup) {
			if (!updateStartupRecord()) {
				mainGUI.fail("Failed to update startup record");
			}
		} else {
			if (!removeStartupRecord()) {
				mainGUI.fail("Failed to delete startup record");
			}
		}
		saveSettings();
	}

	private static final boolean removeStartupRecord() {
		if (hasStartupRecord()) {
			try {
				winRegistry.deleteValue(winRegistry.HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", "SWBF GameInformer");
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return true;
		}
	}

	private static final boolean updateStartupRecord() {
		try {
			winRegistry.writeStringValue(winRegistry.HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", "SWBF GameInformer", "javaw -jar \"" + MAIN.getCurrentFile() + "\"");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static final boolean hasStartupRecord() {
		try {
			String value = winRegistry.readString(winRegistry.HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", "SWBF GameInformer");
			return value != null;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean isStartMinimized() {
		return instance.startMinimized;
	}

	public static void setStartMinimized(boolean startMinimized) {
		instance.startMinimized = startMinimized;
		saveSettings();
	}

	public static String getGamePath() {
		if (instance.gamePath == null) {
			instance.gamePath = detectGamePath();
			saveSettings();
		}
		return instance.gamePath;
	}

	public static String getProfile() {
		if (instance.profile == null) {
			changeProfile(false);
		}
		return instance.profile;
	}

	public static String[] getGameProfiles(String custom) {
		File ff = getProfilesFolder();
		if (ff.exists()) {
			String[] list = ff.list();
			List<String> tmp = new ArrayList<>();
			for (int i = 0; i < list.length; i++) {
				if (list[i].endsWith(".profile")) {
					tmp.add(list[i].substring(0, list[i].length() - ".profile".length()));
				}
			}
			if (custom != null) {
				if (!tmp.contains(custom)) {
					tmp.add(custom);
				}
			}
			list = new String[tmp.size()];
			for (int i = 0, o = tmp.size(); i < o; i++) {
				list[i] = tmp.get(i);
			}
			return list;
		}
		return new String[0];
	}

	private static File getProfilesFolder() {
		return new File(new File(getGamePath()).getParent() + "/SaveGames");
	}

	public static String getRawProfile() {
		return instance.profile;
	}

	public static void changeProfile(boolean mustExist) {
		String[] list = getGameProfiles(instance.profile);
		if (list.length > 0) {
			instance.profile = (String) JOptionPane.showInputDialog(null, "Select profile:", "Profile selection", JOptionPane.PLAIN_MESSAGE, mainGUI.getSWBFWindowIcon(), list, list[0]);
			saveSettings();
		} else if (mustExist) {
			mainGUI.fail("Profile folder does not exist");
		}
	}

	public static void setProfile(String profile) {
		instance.profile = profile;
		if (profile != null) {
			lastProfileUsed = new SavedGame(new File(getProfilesFolder().getAbsolutePath() + "/" + profile + ".profile"));
			lastProfileUsed.setNick(profile);
			lastProfileUsed.save();
		}
		saveSettings();
	}

	public static final void changeGameLocation() {
		instance.gamePath = getGameChoosePath();
		saveSettings();
	}

	public static final String detectGamePath() {
		String v1 = getGameRegistryPath();
		if (v1 == null) {
			v1 = getGameChoosePath();
		}
		return v1;
	}

	private static final String getGameChoosePath() {
		JFileChooser fc = new JFileChooser();
		FileFilter[] filters = fc.getChoosableFileFilters();
		for (FileFilter filter : filters) {
			fc.removeChoosableFileFilter(filter);
		}
		fc.addChoosableFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().equalsIgnoreCase("Battlefront.exe");
			}

			@Override
			public String getDescription() {
				return "Battlefront.exe";
			}

		});
		int ret = fc.showOpenDialog(null);
		if (ret == JFileChooser.APPROVE_OPTION) {
			File f = fc.getSelectedFile();
			if (f != null) {
				if (f.exists()) {
					return f.getAbsolutePath();
				}
			}
		}
		return null;
	}

	private static final String getGameRegistryPath() {
		if (MAIN.isWindows()) {
			try {
				String value = winRegistry.readString(winRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\LucasArts\\Star Wars Battlefront\\1.0", "ExePath");
				if (value != null) {
					return value;
				}
			} catch (Exception e) {
			}
		}
		return null;
	}

	public static void setGamePath(String gamePath) {
		instance.gamePath = gamePath;
		saveSettings();
	}

	public static boolean isHideEmpty() {
		return instance.hideEmpty;
	}

	public static void setHideEmpty(boolean hideEmpty) {
		instance.hideEmpty = hideEmpty;
		saveSettings();
	}

	public static boolean isUpdatePeriodically() {
		return instance.updatePeriodically;
	}

	public static void setUpdatePeriodically(boolean updatePeriodically) {
		instance.updatePeriodically = updatePeriodically;
		saveSettings();
	}

	static {
		loadSettings();
		saveSettings();
	}

	public static boolean isGameWindowed() {
		return instance.gameWindowed;
	}

	public static boolean isGameSkipIntro() {
		return instance.gameSkipIntro;
	}

	public static void setGameWindowed(boolean gameWindowed) {
		instance.gameWindowed = gameWindowed;
		saveSettings();
	}

	public static void setGameSkipIntro(boolean gameSkipIntro) {
		instance.gameSkipIntro = gameSkipIntro;
		saveSettings();
	}

	public static final int getDelay() {
		return instance.delay;
	}

	private Settings() {

	}

	private static void loadSettings() {
		if (new File(MAIN.getCurrentFolder(), settingsFile).exists()) {
			try {
				ObjectInputStream s = new ObjectInputStream(new FileInputStream(new File(MAIN.getCurrentFolder(), settingsFile)));
				Object o = s.readObject();
				s.close();
				if (o instanceof Settings) {
					Settings data = (Settings) o;
					commitChanges(data);
					instance = data;
				}
				instance.loadAtStartup = hasStartupRecord();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			saveSettings();
		}
	}

	private static void saveSettings() {
		try {
			ObjectOutputStream s = new ObjectOutputStream(new FileOutputStream(new File(MAIN.getCurrentFolder(), settingsFile)));
			s.writeObject(instance);
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
