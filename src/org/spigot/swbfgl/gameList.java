package org.spigot.swbfgl;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.List;

import org.spigot.swbfgl.catalog.MapManager;
import org.spigot.swbfgl.gui.mainGUI;
import org.spigot.swbfgl.servers.Server;
import org.spigot.swbfgl.servers.ServerStatus;
import org.spigot.swbfgl.threads.updateThread;

public class gameList {
	private static final Object syncer = new Object();
	// private static String masterSeq = "002D0001030000000073776266726f6e7470630073776266726f6e747063006223273833522b52000000000000";
	private static String masterSeq = "00a00001030000000073776266726f6e7470630073776266726f6e747063006223273833522b4a005c686f73746e616d655c67616d656d6f64655c6d61706e616d655c67616d657665725c6e756d706c61796572735c6d6178706c61796572735c67616d65747970655c73657373696f6e5c7072657673657373696f6e5c737762726567696f6e5c736572766572747970655c70617373776f72640000000000";
	private static gameList instance;
	private final ByteBuffer b = ByteBuffer.allocate(102400);

	private static byte[] getMasterSeq() {
		int len = masterSeq.length();
		byte[] b = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			String hex = masterSeq.substring(i, i + 2);
			int in = i / 2;
			int c = Integer.parseInt(hex, 16);
			b[in] = (byte) c;
		}
		return b;
	}

	public gameList() {
		if (ServerStatus.startListener()) {
			instance = this;
			update();
			new updateThread();
		}
	}

	public static final void update() {
		instance.getMapList();
		instance.getIPListRequest();
	}

	private void getMapList() {
		MapManager.loadExternalMaps();
	}

	private void getIPListRequest() {
		try {
			Socket s = new Socket(Settings.getMasterServerIP(), Settings.getMasterServerPort());
			s.setReuseAddress(true);
			InputStream in = s.getInputStream();
			OutputStream out = s.getOutputStream();
			out.write(getMasterSeq());
			b.position(0);
			s.setSoTimeout(Settings.getMasterServerTimeout());
			try {
				while (true) {
					int i = in.read();
					if (i == -1) {
						break;
					} else {
						b.put((byte) (i));
					}
				}
			} catch (SocketTimeoutException e) {
			}
			in.close();
			out.close();
			s.close();
			int len = b.position();
			byte[] d = b.array();
			if (d.length == 0) {
				mainGUI.fail("Failed to receive data from master server.");
				return;
			}
			byte[] data = new byte[len];
			System.arraycopy(d, 0, data, 0, len);
			List<Server> servers = Decoder.getServers("y3Hd2d", "b#'83R+J", data);

			for (Server ss : servers) {
				if (ss.port >= 0) {
					ServerStatus.getStatus(ss);
				}
			}
			synchronized (syncer) {
				syncer.wait(1000);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
