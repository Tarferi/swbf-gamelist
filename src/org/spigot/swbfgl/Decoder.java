package org.spigot.swbfgl;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.spigot.swbfgl.servers.Server;

public class Decoder {
	private static final class RefObject<T> {
		public T argValue;

		public RefObject(T refArg) {
			argValue = refArg;
		}
	}

	private static class enctypex_data_t {
		public byte[] encxkey = new byte[261];
		public long offset;
		public Long start;
	}

	private static byte[] enctypex_decoder(RefObject<byte[]> key, RefObject<byte[]> validate, RefObject<byte[]> data, RefObject<Long> datalen, RefObject<enctypex_data_t> enctypex_data) {
		byte[] encxkeyb = new byte[261];
		byte[] encxkey = null;
		if (enctypex_data.argValue == null) {
			encxkey = encxkeyb;
		} else {
			encxkey = enctypex_data.argValue.encxkey;
		}
		if (enctypex_data.argValue == null || enctypex_data.argValue.start != null) {
			RefObject<byte[]> tempRef_encxkey = new RefObject<byte[]>(encxkey);
			data.argValue = enctypex_init(tempRef_encxkey, key, validate, data, datalen, enctypex_data);
			encxkey = tempRef_encxkey.argValue;
			if (data.argValue == null) {
				return null;
			}
		}
		if (enctypex_data.argValue == null) {
			RefObject<byte[]> tempRef_encxkey2 = new RefObject<byte[]>(encxkey);
			enctypex_func6(tempRef_encxkey2, data, datalen.argValue);
			encxkey = tempRef_encxkey2.argValue;
			return data.argValue;
		} else if (enctypex_data.argValue.start != null) {
			byte[] temp1 = new byte[(int) (datalen.argValue - enctypex_data.argValue.offset)];
			System.arraycopy(data.argValue, (int) enctypex_data.argValue.offset, temp1, 0, (int) (datalen.argValue - enctypex_data.argValue.offset));
			RefObject<byte[]> tempRef_encxkey3 = new RefObject<byte[]>(encxkey);
			RefObject<byte[]> tempRef_temp1 = new RefObject<byte[]>(temp1);
			long temp3 = enctypex_func6(tempRef_encxkey3, tempRef_temp1, datalen.argValue - enctypex_data.argValue.offset);
			encxkey = tempRef_encxkey3.argValue;
			temp1 = tempRef_temp1.argValue;
			System.arraycopy(temp1, 0, data.argValue, (int) enctypex_data.argValue.offset, (int) (datalen.argValue - enctypex_data.argValue.offset));
			enctypex_data.argValue.offset += temp3;
			byte[] temp2 = new byte[(int) (datalen.argValue - enctypex_data.argValue.start)];
			System.arraycopy(data.argValue, (int) enctypex_data.argValue.start.longValue(), temp2, 0, (int) (datalen.argValue - enctypex_data.argValue.start));
			return temp2;
		}
		return null;
	}

	private static byte[] enctypex_init(RefObject<byte[]> encxkey, RefObject<byte[]> key, RefObject<byte[]> validate, RefObject<byte[]> data, RefObject<Long> datalen, RefObject<enctypex_data_t> enctypex_data) {
		long a = 0;
		long b = 0;
		byte[] encxvalidate = new byte[8];
		if (datalen.argValue < 1) {
			return (null);
		}
		a = (((data.argValue[0] & 0xff) ^ 0xec)) + 2;
		if (datalen.argValue < a) {
			return (null);
		}
		b = (data.argValue[(int) (a - 1)] & 0xff) ^ 0xea;
		if (datalen.argValue < (a + b)) {
			return (null);
		}
		System.arraycopy(validate.argValue, 0, encxvalidate, 0, 8);
		byte[] temp1 = new byte[(int) (datalen.argValue - a + 1)];
		System.arraycopy(data.argValue, (int) a, temp1, (int) 0, (int) (datalen.argValue - a));
		RefObject<byte[]> tempRef_encxvalidate = new RefObject<byte[]>(encxvalidate);
		RefObject<byte[]> tempRef_temp1 = new RefObject<byte[]>(temp1);
		enctypex_funcx(encxkey, key, tempRef_encxvalidate, tempRef_temp1, b);
		encxvalidate = tempRef_encxvalidate.argValue;
		temp1 = tempRef_temp1.argValue;
		System.arraycopy(temp1, 0, data.argValue, (int) a, (int) (datalen.argValue - a));
		a += b;
		enctypex_data.argValue = new enctypex_data_t();
		if (enctypex_data.argValue == null) {
			byte[] temp2 = new byte[(int) (datalen.argValue - a + 1)];
			System.arraycopy(data.argValue, (int) a, temp2, 0, (int) (datalen.argValue - a));
			data.argValue = temp2;
			datalen.argValue -= a;
		} else {
			enctypex_data.argValue.offset = a;
			enctypex_data.argValue.start = a;
		}
		return data.argValue;
	}

	private static long enctypex_func6(RefObject<byte[]> encxkey, RefObject<byte[]> data, long len) {
		long i = 0;
		for (i = 0; i <= len - 1; i++) {
			data.argValue[(int) i] = enctypex_func7(encxkey, data.argValue[(int) i]);
		}
		return (len);
	}

	private static byte enctypex_func7(RefObject<byte[]> encxkey, byte d) {
		int a = 0;
		int b = 0;
		int c = 0;
		a = encxkey.argValue[256] & 0xff;
		b = encxkey.argValue[257] & 0xff;
		c = encxkey.argValue[a & 0xff] & 0xff;
		encxkey.argValue[256] = (byte) ((a + 1) % 256);
		encxkey.argValue[257] = (byte) ((b + c) % 256);
		a = encxkey.argValue[260] & 0xff;
		b = encxkey.argValue[257] & 0xff;
		b = encxkey.argValue[b] & 0xff;
		c = encxkey.argValue[a] & 0xff;
		encxkey.argValue[a] = (byte) b;
		a = encxkey.argValue[259] & 0xff;
		b = encxkey.argValue[257] & 0xff;
		a = encxkey.argValue[a] & 0xff;
		encxkey.argValue[b & 0xff] = (byte) a;
		a = encxkey.argValue[256] & 0xff;
		b = encxkey.argValue[259] & 0xff;
		a = encxkey.argValue[a] & 0xff;
		encxkey.argValue[b] = (byte) a;
		a = encxkey.argValue[256] & 0xff;
		encxkey.argValue[a] = (byte) c;
		b = encxkey.argValue[258] & 0xff;
		a = encxkey.argValue[c] & 0xff;
		c = encxkey.argValue[259] & 0xff;
		b = (b + a) % 256;
		encxkey.argValue[258] = (byte) b;
		a = b;
		c = encxkey.argValue[c] & 0xff;
		b = encxkey.argValue[257] & 0xff;
		b = encxkey.argValue[b] & 0xff;
		a = encxkey.argValue[a] & 0xff;
		c = (c + b) % 256;
		b = encxkey.argValue[260] & 0xff;
		b = encxkey.argValue[b] & 0xff;
		c = (c + b) % 256;
		b = encxkey.argValue[c] & 0xff;
		c = encxkey.argValue[256] & 0xff;
		c = encxkey.argValue[c] & 0xff;
		a = (a + c) % 256;
		c = encxkey.argValue[b] & 0xff;
		b = encxkey.argValue[a] & 0xff;
		encxkey.argValue[260] = d;
		c = (c ^ b ^ (d & 0xff)) % 256;
		encxkey.argValue[259] = (byte) c;
		return (byte) c;
	}

	private static void enctypex_funcx(RefObject<byte[]> encxkey, RefObject<byte[]> key, RefObject<byte[]> encxvalidate, RefObject<byte[]> data, long datalen) {
		long i = 0;
		long keylen = 0;
		keylen = key.argValue.length;
		for (i = 0; i <= datalen - 1; i++) {
			encxvalidate.argValue[(int) ((key.argValue[(int) (i % keylen)] * i) & 7)] = (byte) (encxvalidate.argValue[(int) ((key.argValue[(int) (i % keylen)] * i) & 7)] ^ (encxvalidate.argValue[(int) (i & 7)] & 0xff) ^ (data.argValue[(int) i]) & 0xff);
		}
		enctypex_func4(encxkey, encxvalidate, new RefObject<Long>((long) 8));
	}

	private static void enctypex_func4(RefObject<byte[]> encxkey, RefObject<byte[]> id, RefObject<Long> idlen) {
		long i = 0;
		long n1 = 0;
		long n2 = 0;
		byte t1 = 0;
		byte t2 = 0;
		if (idlen.argValue < 1) {
			return;
		}
		for (i = 0; i <= 255; i++) {
			encxkey.argValue[(int) i] = (byte) i;
		}
		for (i = 255; i >= 0; i += -1) {
			RefObject<Long> tempRef_i = new RefObject<Long>(i);
			RefObject<Long> tempRef_n1 = new RefObject<Long>(n1);
			RefObject<Long> tempRef_n2 = new RefObject<Long>(n2);
			t1 = (byte) enctypex_func5(encxkey, tempRef_i, id, idlen, tempRef_n1, tempRef_n2);
			i = tempRef_i.argValue;
			n1 = tempRef_n1.argValue;
			n2 = tempRef_n2.argValue;
			t2 = encxkey.argValue[(int) i];
			encxkey.argValue[(int) i] = encxkey.argValue[(t1 & 0xff)];
			encxkey.argValue[(t1 & 0xff)] = t2;
		}
		encxkey.argValue[256] = encxkey.argValue[1];
		encxkey.argValue[257] = encxkey.argValue[3];
		encxkey.argValue[258] = encxkey.argValue[5];
		encxkey.argValue[259] = encxkey.argValue[7];
		encxkey.argValue[260] = encxkey.argValue[(int) (n1 & 0xff)];
	}

	private static long enctypex_func5(RefObject<byte[]> encxkey, RefObject<Long> cnt, RefObject<byte[]> id, RefObject<Long> idlen, RefObject<Long> n1, RefObject<Long> n2) {
		long i = 0;
		long tmp = 0;
		long mask = 1;
		if (cnt.argValue == 0) {
			return 0;
		}
		if (cnt.argValue > 1) {
			do {
				mask = (mask << 1) + 1;
			} while (mask < cnt.argValue);
		}
		i = 0;
		do {
			n1.argValue = (long) ((encxkey.argValue[(int) (n1.argValue & 0xff)] & 0xff) + (id.argValue[(int) n2.argValue.longValue()]) & 0xff);
			n2.argValue += 1;
			if (n2.argValue >= idlen.argValue) {
				n2.argValue = (long) 0;
				n1.argValue += idlen.argValue;
			}
			tmp = n1.argValue & mask;
			i += 1;
			if (i > 11) {
				tmp = tmp % cnt.argValue;
			}
		} while (tmp > cnt.argValue);
		return (tmp);
	}

	public static List<Server> getServers(String key, String validate, byte[] data) {
		// saveDump(data);
		RefObject<byte[]> k = new RefObject<>(key.getBytes());
		RefObject<byte[]> v = new RefObject<>(validate.getBytes());
		RefObject<byte[]> d = new RefObject<>(data);
		RefObject<Long> l = new RefObject<>((long) data.length);
		RefObject<enctypex_data_t> p = new RefObject<>(null);
		byte[] t = Decoder.enctypex_decoder(k, v, d, l, p);
		return getServerList(t);
	}

	/*
	 * protected static final void saveDump(byte[] data) { try { new java.io.FileOutputStream(new java.io.File("sck")).write(data); } catch (Exception e) {
	 * 
	 * } }
	 */

	private static String getIP(ByteBuffer b) {
		return (b.get() & 0xff) + "." + (b.get() & 0xff) + "." + (b.get() & 0xff) + "." + (b.get() & 0xff);
	}

	private static int getShort(ByteBuffer b) {
		return (((b.get() & 0xff) << 8) | ((b.get() & 0xff)));
	}

	private static List<Server> getServerList(byte[] Data) {
		try {
			if (Data == null) {
				System.err.println("Failed to fetch data from master server.");
				return new ArrayList<>();
			}
			ByteBuffer b = ByteBuffer.wrap(Data);
			getIP(b);
			getShort(b);
			int sc = b.get();
			for (int i = 0; i < sc; i++) {
				b.get();
				getString(b);
			}
			b.get();
			List<Server> s = new ArrayList<Server>();
			while (true) {
				int type = b.get();
				if (type == 0x15) {
					s.add(getServer(getIP(b), getShort(b)));
				} else if (type == 0x55) {
					s.add(getServer(getIP(b), getShort(b)));
					for (int i = 0; i < sc; i++) {
						b.get();
						getString(b);
					}
				} else if (type == 0x75 || type == 0x7E) {
					List<String> ips = new ArrayList<>();
					while (true) {
						String ip = getIP(b);
						if (ips.contains(ip)) {
							break;
						} else {
							int port = getShort(b);
							s.add(getServer(ip, port));
							ips.add(ip);
						}
					}
					for (int i = 0; i < sc; i++) {
						b.get();
						getString(b);
					}
				} else {
					break;
				}
			}
			return s;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	private final static Object syncer = new Object();
	private static HashMap<String, Server> cache = new HashMap<>();

	private static Server getServer(String ip, int port) {
		synchronized (syncer) {
			String key = ip + ":" + port;
			if (cache.containsKey(key)) {
				return cache.get(key);
			} else {
				Server s = new Server(ip, port);
				cache.put(key, s);
				return s;
			}
		}
	}

	private static String getString(ByteBuffer b) {
		StringBuilder sb = new StringBuilder();
		while (true) {
			byte c = b.get();
			if (c == 0) {
				break;
			} else {
				sb.append((char) c);
			}
		}
		return sb.toString();
	}
}
